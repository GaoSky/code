//
//  CF372C Watching Fireworks is Fun.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/29.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#define re register
#define maxn 100
using namespace std;
int n,m,d;
template <class T>
inline void read(T &x)
{
    T op=1;x=0;char c=getchar();
    while(c<'0'||c>'9')
    {
        if(c=='-')
            op=-1;
        c=getchar();
    }
    while(c>='0'&&c<='9')
        x=x*10+c-'0',c=getchar();
}
struct Firework
{
    int a,b,t;
}a[maxn];
long long f[3][maxn],ans=0x7fffffffffffffff,sum;
long long len;
int q[maxn],l,r;
int main()
{
    read(n),read(m),read(d);
    for(re int i=1;i<=m;++i)
        read(a[i].a),read(a[i].b),read(a[i].t),sum+=a[i].b;
    int now,last;
    for(re int i=1;i<=m;++i)
    {
        now=i&1,last=now^1;l=1,r=0;
        len=(a[i].t-a[i-1].t)*d;
        for(re int j=1;j<=n;++j)
        {
            while(r>=l&&q[l]<j-len)
                l++;
            while(r>=l&&f[last][q[r]]>f[last][j])
                r--;
            q[++r]=j;
            f[now][j]=f[last][q[r]]+abs(j-a[i].a);
        }l=1,r=0;
        for(re int j=n;j>=1;--j)
        {
            while(r>=l&&q[l]>j+len)
                ++l;
            while(r>=l&&f[last][q[r]]>f[last][j])
                r--;
            q[++r]=j;
            f[now][j]=min(f[now][j],f[last][q[r]]+abs(j-a[i].a));
        }
    }
    for(re int i=1;i<=n;++i)
        ans=min(ans,f[now][i]);
    printf("%lld",sum-ans);
    return 0;
}
