//
//  C24-分治II-树的统计[树链剖分].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/6/20.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#define re register
#define maxn 30005
#define inf 0x7fffffff
using namespace std;
int n,q,tim;
int val[maxn],dep[maxn],size[maxn];
int fa[maxn],dfn[maxn],top[maxn];
vector<int> son[maxn];
struct SegmentTree
{
    int l,r,mx,sum;
}tr[maxn*4];
inline void addedge(int x,int y)
{
    son[x].push_back(y);
}
void dfs1(int x)
{
    size[x]=1;
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(y==fa[x])
            continue;
        dep[y]=dep[x]+1,fa[y]=x;
        dfs1(y);
        size[x]+=size[y];
    }
}
void dfs2(int x,int k)
{
    int Heavy=0;
    dfn[x]=++tim,top[x]=k;
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(dep[y]>dep[x]&&size[y]>size[Heavy])
            Heavy=y;
    }
    if(Heavy==0)
        return;
    dfs2(Heavy,k);
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(dep[y]>dep[x]&&y!=Heavy)
            dfs2(y,y);
    }
}
void buildtree(int x,int l,int r)
{
    tr[x].l=l,tr[x].r=r;
    if(l==r)
        return;
    int mid=(l+r)>>1;
    buildtree(x<<1,l,mid);
    buildtree(x<<1|1,mid+1,r);
}
void modify(int k,int x,int y)
{
    int l=tr[k].l,r=tr[k].r,mid=(l+r)>>1;
    if(l==r)
    {
        tr[k].sum=tr[k].mx=y;
        return;
    }
    if(x<=mid)
        modify(k<<1,x,y);
    else
        modify(k<<1|1,x,y);
    tr[k].sum=tr[k<<1].sum+tr[k<<1|1].sum;
    tr[k].mx=max(tr[k<<1].mx,tr[k<<1|1].mx);
}
int Query1(int k,int x,int y)
{
    int l=tr[k].l,r=tr[k].r,mid=(l+r)>>1;
    if(l==x&&y==r)
        return tr[k].sum;
    if(y<=mid)
        return Query1(k<<1,x,y);
    else if(x>mid)
        return Query1(k<<1|1,x,y);
    else
        return Query1(k<<1,x,mid)+Query1(k<<1|1,mid+1,y);
}
int Query2(int k,int x,int y)
{
    int l=tr[k].l,r=tr[k].r,mid=(l+r)>>1;
    if(l==x&&y==r)
        return tr[k].mx;
    if(y<=mid)
        return Query2(k<<1,x,y);
    else if(x>mid)
        return Query2(k<<1|1,x,y);
    else
        return max(Query2(k<<1,x,mid),Query2(k<<1|1,mid+1,y));
}
int GetSum(int x,int y)
{
    int sum=0;
    while(top[x]!=top[y])
    {
        if(dep[top[x]]<dep[top[y]])
            swap(x,y);
        sum+=Query1(1,dfn[top[x]],dfn[x]);
        x=fa[top[x]];
    }
    if(dfn[x]>dfn[y])
        swap(x,y);
    sum+=Query1(1,dfn[x],dfn[y]);
    return sum;
}
int GetMax(int x,int y)
{
    int mx=-inf;
    while(top[x]!=top[y])
    {
        if(dep[top[x]]<dep[top[y]])
            swap(x,y);
        mx=max(mx,Query2(1,dfn[top[x]],dfn[x]));
        x=fa[top[x]];
    }
    if(dfn[x]>dfn[y])
    swap(x,y);
    mx=max(mx,Query2(1,dfn[x],dfn[y]));
    return mx;
}
int main()
{
    scanf("%d",&n);
    for(re int i=1;i<n;++i)
    {
        int x,y;
        scanf("%d%d",&x,&y);
        addedge(x,y),addedge(y,x);
    }
    for(re int i=1;i<=n;++i)
        scanf("%d",&val[i]);
    dfs1(1);
    dfs2(1,1);
    buildtree(1,1,n);
    for(re int i=1;i<=n;++i)
        modify(1,dfn[i],val[i]);
    scanf("%d",&q);
    char op[10];int x,y;
    while(q--)
    {
        scanf("%s",op),scanf("%d%d",&x,&y);
        if(op[0]=='C')
            val[x]=y,modify(1,dfn[x],y);
        else if(op[1]=='M')
            printf("%d\n",GetMax(x,y));
        else
            printf("%d\n",GetSum(x,y));
    }
    return 0;
}
