//
//  C12-二叉树II-整数问题[线段树延迟标记].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/18.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
struct Tree
{
    int l,r,mark;
    long long val;
}t[maxn*4];
long long a[maxn];int n,q;
void buildtree(int x,int l,int r)
{
    t[x].l=l,t[x].r=r;
    if(l==r)
    {
        t[x].val=a[l];
        return;
    }
    int mid=(l+r)>>1;
    int lch=x*2,rch=x*2+1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
    t[x].val=t[lch].val+t[rch].val;
}
void release(int x)
{
    if(t[x].mark&&t[x].l<t[x].r)
    {
        int lch=x*2,rch=x*2+1;
        t[lch].val+=t[x].mark*(t[lch].r-t[lch].l+1);
        t[lch].mark+=t[x].mark;
        t[rch].val+=t[x].mark*(t[rch].r-t[rch].l+1);
        t[rch].mark+=t[x].mark;
    }
    t[x].mark=0;
}
void modify(int x,int l,int r,int k)
{
    release(x);
    if(l<=t[x].l&&r>=t[x].r)
    {
        t[x].val+=k*(t[x].r-t[x].l+1);
        t[x].mark+=k;
        return;
    }
    int mid=(t[x].l+t[x].r)>>1;
    int lch=x*2,rch=x*2+1;
    if(l<=mid)
        modify(lch,l,r,k);
    if(r>mid)
        modify(rch,l,r,k);
    t[x].val=t[lch].val+t[rch].val;
}
long long query(int x,int l,int r)
{
    release(x);
    if(l<=t[x].l&&r>=t[x].r)
        return t[x].val;
    int mid=(t[x].l+t[x].r)>>1;
    int lch=x*2,rch=x*2+1;
    long long res=0;
    if(l<=mid)
        res+=query(lch,l,r);
    if(r>mid)
        res+=query(rch,l,r);
    return res;
}
int main()
{
    scanf("%d%d",&n,&q);
    for(re int i=1;i<=n;++i)
        scanf("%lld",&a[i]);
    buildtree(1,1,n);
    char op[10];int x,y,z;
    while(q--)
    {
        scanf("%s",op);
        if(op[0]=='C')
        {
            scanf("%d%d%d",&x,&y,&z);
            modify(1,x,y,z);
        }
        else if(op[0]=='Q')
        {
            scanf("%d%d",&x,&y);
            printf("%lld\n",query(1,x,y));
        }
    }
    return 0;
}
