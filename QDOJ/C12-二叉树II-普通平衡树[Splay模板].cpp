//
//  C12-二叉树II-普通平衡树[Splay模板].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/30.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
struct Node
{
    int l,r,val,cnt,size;
}a[maxn];
int tot,root;
inline void Update(int x)
{
    a[x].size=a[a[x].l].size+a[a[x].r].size+a[x].cnt;
}
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
    Update(a[x].r),Update(x);
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
    Update(a[x].l),Update(x);
}
void Create(int &now,int val)
{
    now=++tot;
    a[tot].val=val;
    a[tot].cnt++;
    a[tot].size++;
}
void Splay(int x,int &y)
{
    if(x==y)
        return;
    int &l=a[y].l,&r=a[y].r;
    if(x==l)
        zig(y);
    else if(x==r)
        zag(y);
    else
    {
        if(a[x].val<a[y].val)
        {
            if(a[x].val<a[l].val)
                Splay(x,a[l].l),zig(y),zig(y);
            else
                Splay(x,a[l].r),zag(l),zig(y);
        }
        else
        {
            if(a[x].val>a[r].val)
                Splay(x,a[r].r),zag(y),zag(y);
            else
                Splay(x,a[r].l),zig(r),zag(y);
        }
    }
}
void insert(int &now,int &val)
{
    if(now==0)
    {
        Create(now,val);
        Splay(now,root);
        return;
    }
    else if(val<a[now].val)
        insert(a[now].l,val);
    else if(val>a[now].val)
        insert(a[now].r,val);
    else
    {
        a[now].cnt++;
        a[now].size++;
        Splay(now,root);
    }
}
void _Delete(int now)
{
    Splay(now,root);
    if(a[now].cnt>1)
        a[now].cnt--,a[now].size--;
    else if(a[now].r)
    {
        int p=a[now].r;
        while(a[p].l)
            p=a[p].l;
        Splay(p,a[now].r);
        a[a[now].r].l=a[now].l;
        root=a[now].r;
        Update(root);
    }
    else
        root=a[root].l;
}
void Erase(int now,int val)
{
    if(a[now].val==val)
        _Delete(now);
    else if(val<a[now].val)
        Erase(a[now].l,val);
    else
        Erase(a[now].r,val);
}
int Query1(int val)
{
    int now=root,rank=1;
    while(now)
    {
        if(a[now].val==val)
        {
            rank+=a[a[now].l].size;
            Splay(now,root);
            break;
        }
        else if(val<=a[now].val)
            now=a[now].l;
        else
            rank+=a[a[now].l].size+a[now].cnt,now=a[now].r;
    }
    return rank;
}
int Query2(int rank)
{
    int now=root;
    while(now)
    {
        int lsize=a[a[now].l].size;
        if(lsize+1<=rank&&rank<=lsize+a[now].cnt)
        {
            Splay(now,root);
            break;
        }
        else if(lsize>=rank)
            now=a[now].l;
        else
            rank=rank-a[a[now].l].size-a[now].cnt,now=a[now].r;
    }
    return a[now].val;
}
int main()
{
    int t,op,x;
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d%d",&op,&x);
        if(op==1)
            insert(root,x);
        else if(op==2)
            Erase(root,x);
        else if(op==3)
            printf("%d\n",Query1(x));
        else if(op==4)
            printf("%d\n",Query2(x));
        else if(op==5)
            printf("%d\n",Query2(Query1(x)-1));
        else if(op==6)
            printf("%d\n",Query2(Query1(x+1)));
    }
    return 0;
}
