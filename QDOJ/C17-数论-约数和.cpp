//
//  C17-数论-约数和.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/13.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 50005
#define ll long long
using namespace std;
int n,tot,cnt,ans[maxn*2],prime[maxn];
bool vis[maxn];
void get_prime()
{
    for(re int i=2;i<maxn;++i)
    {
        if(!vis[i])
            prime[++tot]=i;
        for(re int j=1;j<=tot&&prime[j]*i<maxn;++j)
        {
            vis[prime[j]*i]=true;
            if(i%prime[j]==0)
                break;
        }
    }
    vis[1]=true;
}

inline bool is_prime(ll x)
{
    if(x<maxn)
        return 1-vis[x];
    for(re ll i=2;i*i<=x;++i)
        if(x%i==0)
            return false;
    return true;
}

inline void dfs(ll newS,int pos,ll multi)
{
    if(newS==1)
    {
        ans[++cnt]=multi;
        return;
    }
    if(newS-1>=prime[pos]&&is_prime(newS-1))
        ans[++cnt]=(newS-1)*multi;
    for(re int i=pos;i<=tot&&prime[i]*prime[i]<=newS;++i)
        for(re ll tmp=prime[i],j=prime[i]+1;j<=newS;tmp*=prime[i],j+=tmp)
            if(newS%j==0)
                dfs(newS/j,i+1,multi*tmp);
}

int main()
{
    ll S;
    get_prime();
    while(scanf("%lld",&S)!=EOF)
    {
        cnt=0;
        dfs(S,1,1);
        sort(ans+1,ans+cnt+1);
        printf("%d\n",cnt);
        for(re int i=1;i<=cnt;++i)
            printf("%d ",ans[i]);
        if(cnt)
            putchar('\n');
    }
    return 0;
}
