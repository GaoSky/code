//
//  C12-二叉树II-亚特兰蒂斯[线段树扫描线].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/18.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#define re register
#define maxn 105
using namespace std;
struct Node
{
    double l,r,h;
    int f;
};
struct Tree
{
    int l,r,s;
    double len;
}t[maxn*9];
vector<Node> e;
vector<double> book;
int n;
bool cmp(Node a,Node b)
{
    return a.h<b.h;
}
void buildtree(int x,int l,int r)
{
    t[x].l=l,t[x].r=r,t[x].s=t[x].len=0;
    if(l==r)
        return;
    int mid=(l+r)>>1;
    int lch=x*2,rch=x*2+1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
}
void pushup(int x)
{
    if(t[x].s)
        t[x].len=book[t[x].r+1]-book[t[x].l];
    else if(t[x].l==t[x].r)
        t[x].len=0;
    else
        t[x].len=t[x*2].len+t[x*2+1].len;
}
void modify(int x,int l,int r,int k)
{
    if(l<=t[x].l&&r>=t[x].r)
    {
        t[x].s+=k;
        pushup(x);
        return;
    }
    int mid=(t[x].l+t[x].r)>>1;
    int lch=x*2,rch=x*2+1;
    if(l<=mid)
        modify(lch,l,r,k);
    if(r>mid)
        modify(rch,l,r,k);
    pushup(x);
}
int main()
{
    int kase=0;
    double x1,x2,y1,y2;
    while(scanf("%d",&n)!=EOF)
    {
        if(n==0)
            return 0;
        book.clear(),e.clear();
        for(re int i=1;i<=n;++i)
        {
            scanf("%lf%lf%lf%lf",&x1,&y1,&x2,&y2);
            e.push_back((Node){x1,x2,y1,-1});
            e.push_back((Node){x1,x2,y2,1});
            book.push_back(x1),book.push_back(x2);
        }
        sort(e.begin(),e.end(),cmp);
        sort(book.begin(),book.end());
        book.erase(unique(book.begin(),book.end()),book.end());
        int m=e.size(),k=book.size();
        buildtree(1,0,k);
        double ans=0.0;
        for(re int i=0;i<m;++i)
        {
            int l=lower_bound(book.begin(),book.end(),e[i].l)-book.begin();
            int r=lower_bound(book.begin(),book.end(),e[i].r)-book.begin()-1;
            modify(1,l,r,e[i].f);
            ans+=t[1].len*(e[i+1].h-e[i].h);
        }
        printf("Test case #%d\n",++kase);
        printf("Total explored area: %.2lf\n\n",ans);
    }
    return 0;
}
