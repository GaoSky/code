//
//  C12-二叉树II-营业额统计[Treap].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/27.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 32770
#define inf 0x7fffffff
using namespace std;
struct Treap
{
    int l,r,val,data,cnt,size;
}a[maxn];
int tot,root,n;
inline void Create(int val)
{
    a[++tot].val=val;
    a[tot].data=rand();
    a[tot].cnt=a[tot].size=1;
}
inline void Update(int x)
{
    a[x].size=a[a[x].l].size+a[a[x].r].size+a[x].cnt;
}
inline void init()
{
    Create(-inf),Create(inf);
    root=1;
    a[1].r=2;
    Update(root);
}
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
    Update(a[x].r),Update(x);
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
    Update(a[x].l),Update(x);
}
inline void insert(int &x,int val)
{
    if(x==0)
    {
        Create(val),x=tot;
        return;
    }
    if(val==a[x].val)
    {
        a[x].cnt++,Update(x);
        return;
    }
    else if(val<a[x].val)
    {
        insert(a[x].l,val);
        if(a[x].data<a[a[x].l].data)
            zig(x);
    }
    else
    {
        insert(a[x].r,val);
        if(a[x].data<a[a[x].r].data)
            zag(x);
    }
    Update(x);
}
inline int Query1(int &x,int val)
{
    if(x==0)
        return 0;
    if(val==a[x].val)
        return a[a[x].l].size+1;
    if(val<a[x].val)
        return Query1(a[x].l,val);
    else
        return Query1(a[x].r,val)+a[a[x].l].size+a[x].cnt;
}
inline int Query2(int &x,int val)
{
    if(x==0)
        return inf;
    if(a[a[x].l].size>=val)
        return Query2(a[x].l,val);
    if(a[a[x].l].size+a[x].cnt>=val)
        return a[x].val;
    return Query2(a[x].r,val-a[a[x].l].size-a[x].cnt);
}
int main()
{
    srand((unsigned)time(NULL));
    init();
    int tmp,tmp1,tmp2;
    long long ans=0;
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
    {
        tmp1=tmp2=inf;
        scanf("%d",&tmp);
        insert(root,tmp);
        if(i==1)
            ans+=tmp;
        else
        {
            int rk=Query1(root,tmp);
            tmp1=Query2(root,rk-1);
            if(tmp1==-inf)
                tmp1=inf;
            tmp2=Query2(root,rk+1);
            ans+=min(abs(tmp1-tmp),abs(tmp2-tmp));
        }
    }
    printf("%lld",ans);
    return 0;
}
