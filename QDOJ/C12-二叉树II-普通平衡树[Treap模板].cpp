//
//  C12-二叉树II-普通平衡树[Treap模板].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/27.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
#define inf 0x7fffffff
using namespace std;
struct Treap
{
    int l,r,val,data,cnt,size;
}a[maxn];
int tot,root,n;
inline void Update(int x)
{
    a[x].size=a[a[x].l].size+a[a[x].r].size+a[x].cnt;
}
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
    Update(a[x].r),Update(x);
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
    Update(a[x].l),Update(x);
}
void Create(int val)
{
    a[++tot].val=val;
    a[tot].data=rand();
    a[tot].cnt=a[tot].size=1;
}
void init()
{
    Create(-inf),Create(inf);
    root=1;
    a[1].r=2;
    Update(root);
}
void insert(int &x,int val)
{
    if(x==0)
    {
        Create(val),x=tot;
        return;
    }
    if(val==a[x].val)
    {
        a[x].cnt++,Update(x);
        return;
    }
    else if(val<a[x].val)
    {
        insert(a[x].l,val);
        if(a[x].data<a[a[x].l].data)
            zig(x);
    }
    else
    {
        insert(a[x].r,val);
        if(a[x].data<a[a[x].r].data)
            zag(x);
    }
    Update(x);
}
void _Delete(int &x,int val)
{
    if(x==0)
        return;
    if(val==a[x].val)
    {
        if(a[x].cnt>1)
        {
            a[x].cnt--,Update(x);
            return;
        }
        if(a[x].l||a[x].r)
        {
            if(a[x].r==0||a[a[x].l].data>a[a[x].r].data)
                zig(x),_Delete(a[x].r,val);
            else
                zag(x),_Delete(a[x].l,val);
            Update(x);
        }
        else
            x=0;
        return;
    }
    if(val<a[x].val)
        _Delete(a[x].l,val);
    else
        _Delete(a[x].r,val);
    Update(x);
}
int Query1(int &x,int val)
{
    if(x==0)
        return 0;
    if(val==a[x].val)
        return a[a[x].l].size+1;
    if(val<a[x].val)
        return Query1(a[x].l,val);
    else
        return Query1(a[x].r,val)+a[a[x].l].size+a[x].cnt;
}
int Query2(int &x,int val)
{
    if(x==0)
        return inf;
    if(a[a[x].l].size>=val)
        return Query2(a[x].l,val);
    if(a[a[x].l].size+a[x].cnt>=val)
        return a[x].val;
    return Query2(a[x].r,val-a[a[x].l].size-a[x].cnt);
}
int PreNode(int val)
{
    int ans=1,x=root;
    while(x)
    {
        if(val==a[x].val)
        {
            if(a[x].l>0)
            {
                x=a[x].l;
                while(a[x].r>0)
                    x=a[x].r;
                ans=x;
            }
            break;
        }
        if(a[x].val<val&&a[x].val>a[ans].val)
            ans=x;
        if(val<a[x].val)
            x=a[x].l;
        else
            x=a[x].r;
    }
    return a[ans].val;
}
int NextNode(int val)
{
    int ans=2,x=root;
    while(x)
    {
        if(val==a[x].val)
        {
            if(a[x].r>0)
            {
                x=a[x].r;
                while(a[x].l>0)
                    x=a[x].l;
                ans=x;
            }
            break;
        }
        if(a[x].val>val&&a[x].val<a[ans].val)
            ans=x;
        if(val<a[x].val)
            x=a[x].l;
        else
            x=a[x].r;
    }
    return a[ans].val;
}
int main()
{
    srand((unsigned)time(NULL));
    init();
    int opt,x;
    scanf("%d",&n);
    while(n--)
    {
        scanf("%d%d",&opt,&x);
        if(opt==1)
            insert(root,x);
        if(opt==2)
            _Delete(root,x);
        if(opt==3)
            printf("%d\n",Query1(root,x)-1);
        if(opt==4)
            printf("%d\n",Query2(root,x+1));
        if(opt==5)
            printf("%d\n",PreNode(x));
        if(opt==6)
            printf("%d\n",NextNode(x));
    }
    return 0;
}
