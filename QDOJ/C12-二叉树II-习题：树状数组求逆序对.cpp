//
//  C12-二叉树II-习题：树状数组求逆序对.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/11.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
#define lowbit(x) x&(-x)
using namespace std;
int t[maxn],book[maxn],n;
long long ans;
struct Num
{
    int pos,val;
}a[maxn];
bool cmp(Num a,Num b)
{
    if(a.val==b.val)
        return a.pos<b.pos;
    return a.val<b.val;
}
void add(int x)
{
    for(;x<=n;x+=lowbit(x))
        t[x]++;
}
int query(int x)
{
    int res=0;
    for(;x;x-=lowbit(x))
        res+=t[x];
    return res;
}
int main()
{
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
        scanf("%d",&a[i].val),a[i].pos=i;
    sort(a+1,a+n+1,cmp);
    for(re int i=1;i<=n;++i)
        book[a[i].pos]=i;
    for(re int i=1;i<=n;++i)
    {
        add(book[i]);
        ans+=i-query(book[i]);
    }
    printf("%lld",ans);
    return 0;
}
