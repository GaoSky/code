//
//  C23-分块-简单整数问题[分块].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/4.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <cmath>
#define re register
#define maxn 100005
using namespace std;
int n,q;
int add[maxn];
int pos[maxn],a[maxn];
int l[maxn],r[maxn];
long long sum[maxn];
void modify(int x,int y,int z)
{
    int p=pos[x],q=pos[y];
    if(p==q)
    {
        for(re int i=x;i<=y;++i)
            a[i]+=z;
        sum[p]+=z*(y-x+1);
    }
    else
    {
        for(re int i=p+1;i<=q-1;++i)
            add[i]+=z;
        for(re int i=x;i<=r[p];++i)
            a[i]+=z;
        sum[p]+=z*(r[p]-x+1);
        for(re int i=l[q];i<=y;++i)
            a[i]+=z;
        sum[q]+=z*(y-l[q]+1);
    }
}
long long query(int x,int y)
{
    long long ans=0;
    int p=pos[x],q=pos[y];
    if(p==q)
    {
        for(re int i=x;i<=y;++i)
            ans+=a[i];
        ans+=add[p]*(y-x+1);
    }
    else
    {
        for(re int i=p+1;i<=q-1;++i)
            ans+=(sum[i]+add[i]*(r[i]-l[i]+1));
        for(re int i=x;i<=r[p];++i)
            ans+=a[i];
        ans+=add[p]*(r[p]-x+1);
        for(re int i=l[q];i<=y;++i)
            ans+=a[i];
        ans+=add[q]*(y-l[q]+1);
    }
    return ans;
}
int main()
{
    scanf("%d%d",&n,&q);
    for(re int i=1;i<=n;++i)
        scanf("%d",&a[i]);
    int k=sqrt(n),t=n/k;
    for(re int i=1;i<=t;++i)
        l[i]=(i-1)*k+1,r[i]=i*k;
    if(r[t]<n)
    {
        t++;
        l[t]=r[t-1]+1;
        r[t]=n;
    }
    for(re int i=1;i<=t;++i)
        for(re int j=l[i];j<=r[i];++j)
            pos[j]=i,sum[i]+=a[j];
    int x,y,z;
    while(q--)
    {
        char op[2];
        scanf("%s",op);
        if(op[0]=='C')
        {
            scanf("%d%d%d",&x,&y,&z);
            modify(x,y,z);
        }
        else
        {
            scanf("%d%d",&x,&y);
            printf("%lld\n",query(x,y));
        }
    }
    return 0;
}
