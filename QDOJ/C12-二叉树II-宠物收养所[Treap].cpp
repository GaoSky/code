//
//  C12-二叉树II-宠物收养所[Treap].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/27.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 80005
#define inf 0x7fffffff
using namespace std;
struct Treap
{
    int l,r,val,data;
}a[maxn];
int tot,root,n,status;
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
}
inline void Create(int val)
{
    a[++tot].val=val;
    a[tot].data=rand();
}
inline void init()
{
    Create(-inf),Create(inf);
    root=1;
    a[1].r=2;
}
inline void insert(int &x,int val)
{
    if(x==0)
    {
        Create(val),x=tot;
        return;
    }
    if(val<a[x].val)
    {
        insert(a[x].l,val);
        if(a[x].data<a[a[x].l].data)
            zig(x);
    }
    else
    {
        insert(a[x].r,val);
        if(a[x].data<a[a[x].r].data)
            zag(x);
    }
}
void _Delete(int &x,int val)
{
    if(x==0)
        return;
    if(val==a[x].val)
    {
        if(a[x].l||a[x].r)
        {
            if(a[x].r==0||a[a[x].l].data>a[a[x].r].data)
                zig(x),_Delete(a[x].r,val);
            else
                zag(x),_Delete(a[x].l,val);
        }
        else
            x=0;
        return;
    }
    if(val<a[x].val)
        _Delete(a[x].l,val);
    else
        _Delete(a[x].r,val);
}
int PreNode(int val)
{
    int ans=1,x=root;
    while(x)
    {
        if(val==a[x].val)
        {
            if(a[x].l>0)
            {
                x=a[x].l;
                while(a[x].r>0)
                    x=a[x].r;
                ans=x;
            }
            break;
        }
        if(a[x].val<val&&a[x].val>a[ans].val)
            ans=x;
        if(val<a[x].val)
            x=a[x].l;
        else
            x=a[x].r;
    }
    return a[ans].val;
}
int NextNode(int val)
{
    int ans=2,x=root;
    while(x)
    {
        if(val==a[x].val)
        {
            if(a[x].r>0)
            {
                x=a[x].r;
                while(a[x].l>0)
                    x=a[x].l;
                ans=x;
            }
            break;
        }
        if(a[x].val>val&&a[x].val<a[ans].val)
            ans=x;
        if(val<a[x].val)
            x=a[x].l;
        else
            x=a[x].r;
    }
    return a[ans].val;
}
bool find(int &x,int val)
{
    if(x==0)
        return false;
    if(val==a[x].val)
        return true;
    if(val<a[x].val)
        return find(a[x].l,val);
    else
        return find(a[x].r,val);
}
int main()
{
    srand((unsigned)time(NULL));
    init();int op,x,ans=0;
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
    {
        scanf("%d%d",&op,&x);
        if(status>0)
        {
            if(op==0)
                insert(root,x);
            else
            {
                int tmp1=PreNode(x),tmp2=NextNode(x);
                if(tmp1==-inf&&tmp2==inf)
                    continue;
                if(tmp1==-inf)
                {
                    if(find(root,x))
                        _Delete(root,x);
                    else
                    {
                        ans+=(tmp2-x);
                        _Delete(root,tmp2);
                    }
                }
                else if(tmp2==inf)
                {
                    if(find(root,x))
                        _Delete(root,x);
                    else
                    {
                        ans+=(x-tmp1);
                        _Delete(root,tmp1);
                    }
                }
                else if(x-tmp1<=tmp2-x)
                {
                    ans+=(x-tmp1);
                    _Delete(root,tmp1);
                }
                else
                {
                    ans+=(tmp2-x);
                    _Delete(root,tmp2);
                }
            }
        }
        else if(status==0)
            insert(root,x);
        else
        {
            if(op==1)
                insert(root,x);
            else
            {
                int tmp1=PreNode(x),tmp2=NextNode(x);
                if(tmp1==-inf&&tmp2==inf)
                    continue;
                if(tmp1==-inf)
                {
                    if(find(root,x))
                        _Delete(root,x);
                    else
                    {
                        ans+=(tmp2-x);
                        _Delete(root,tmp2);
                    }
                }
                else if(tmp2==inf)
                {
                    if(find(root,x))
                        _Delete(root,x);
                    else
                    {
                        ans+=(x-tmp1);
                        _Delete(root,tmp1);
                    }
                }
                else if(x-tmp1<=tmp2-x)
                {
                    ans+=(x-tmp1);
                    _Delete(root,tmp1);
                }
                else
                {
                    ans+=(tmp2-x);
                    _Delete(root,tmp2);
                }
            }
        }
        status+=(op==0?1:-1);
        ans=ans%1000000;
    }
    printf("%d",ans);
    return 0;
}
