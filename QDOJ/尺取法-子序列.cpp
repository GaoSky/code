//
//  尺取法-子序列.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/4.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
using namespace std;
int n,S;
int a[100005];
int main()
{
    int t;scanf("%d",&t);
    while(t--)
    {
        scanf("%d%d",&n,&S);
        for(re int i=1;i<=n;++i)
            scanf("%d",&a[i]);
        int s,t,sum,res=n+1;
        s=t=1,sum=0;
        while(true)
        {
            while(sum<S&&t<=n)
                sum+=a[t++];
            if(sum<S)
                break;
            res=min(res,t-s);
            sum-=a[s++];
        }
        if(res>n)
            printf("0\n");
        else
            printf("%d\n",res);
    }
    return 0;
}
