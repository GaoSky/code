//
//  C12-二叉树II-文艺平衡树[Splay].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/30.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define inf 0x7fffffff
#define maxn 100005
using namespace std;
int n,m;
int cnt,root;
struct Node
{
    int fa,son[2],size,val;
    bool tag;
}a[maxn];
void Update(int x)
{
    if(x)
    {
        a[x].size=1;
        if(a[x].son[0])
            a[x].size+=a[a[x].son[0]].size;
        if(a[x].son[1])
            a[x].size+=a[a[x].son[1]].size;
    }
}
void Release(int x)
{
    if(a[x].tag)
    {
        if(a[x].son[0])
        {
            a[a[x].son[0]].tag^=true;
            swap(a[a[x].son[0]].son[0],a[a[x].son[0]].son[1]);
        }
        if(a[x].son[1])
        {
            a[a[x].son[1]].tag^=true;
            swap(a[a[x].son[1]].son[0],a[a[x].son[1]].son[1]);
        }
        a[x].tag=false;
    }
}
void Rotate(int x,int op)
{
    int y=a[x].fa,z=a[y].fa;
    int B=a[x].son[op],C=a[x].son[!op];
    a[x].son[op]=y;
    a[x].fa=z;
    a[y].son[!op]=B;
    a[y].fa=x;
    a[z].son[a[z].son[1]==y]=x;
    a[B].fa=y;
    Update(y);
    Update(x);
}
void Splay(int x,int goal)
{
    if(x==goal)
        return;
    while(a[x].fa!=goal)
    {
        int y=a[x].fa,z=a[y].fa;
        Release(z),Release(y),Release(x);
        bool flag1=(a[y].son[1]==x),flag2=(a[z].son[1]==y);
        if(z==goal)
            Rotate(x,!flag1);
        else
           {
               if(flag1==flag2)
                   Rotate(x,!flag2);
               else
                   Rotate(x,!flag1);
               Rotate(x,!flag2);
           }
    }
    if(goal==0)
        root=x;
}
int Query(int x)
{
    int now=root;
    Release(x);
    while(a[a[now].son[0]].size+1!=x)
    {
        if(a[a[now].son[0]].size+1>x)
            now=a[now].son[0];
        else
        {
            x-=a[a[now].son[0]].size+1;
            now=a[now].son[1];
        }
        Release(now);
    }
    return now;
}
inline void REVERSE(int l,int r)
{
    Splay(Query(l-1),0);
    Splay(Query(r+1),root);
    int t=a[a[root].son[1]].son[0];
    a[t].tag^=true;
    swap(a[t].son[0],a[t].son[1]);
}
int build(int l,int r,int fa)
{
    if(l>r)
        return 0;
    int mid=(l+r)>>1,x=++cnt;
    a[x].val=mid-1;
    a[x].size=1;
    a[x].fa=fa;
    a[x].tag=false;
    a[x].son[0]=build(l,mid-1,x);
    a[x].son[1]=build(mid+1,r,x);
    Update(x);
    return x;
}
void print(int x)
{
    Release(x);
    if(a[x].son[0])
        print(a[x].son[0]);
    if(a[x].val>=1&&a[x].val<=n)
        printf("%d ",a[x].val);
    if(a[x].son[1])
        print(a[x].son[1]);
}
int main()
{
    int x,y;
    scanf("%d%d",&n,&m);
    root=build(1,n+2,0);
    while(m--)
    {
        scanf("%d%d",&x,&y);
        REVERSE(x+1,y+1);
    }
    print(root);
    return 0;
}
