//
//  C12-二叉树II-用二叉堆实现优先队列.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/27.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 500005
using namespace std;
int heap[maxn],h;
void insert(int x)
{
    heap[++h]=x;
    int m=h;
    while(m>=2&&heap[m]>heap[m/2])
        swap(heap[m],heap[m/2]),m/=2;
}
void heapify(int x)
{
    int large=x;
    int lch=x*2,rch=x*2+1;
    if(lch<=h&&heap[lch]>heap[x])
        large=lch;
    if(rch<=h&&heap[rch]>heap[large])
        large=rch;
    if(x!=large)
        swap(heap[large],heap[x]),heapify(large);
}
void extract()
{
    printf("%d\n",heap[1]);
    heap[1]=heap[h--];
    heapify(1);
}
int main()
{
    int x;char op[10];
    while(scanf("%s",op)!=EOF)
    {
        if(op[2]=='d')
            return 0;
        if(op[0]=='i')
        {
            scanf("%d",&x);
            insert(x);
        }
        else if(op[0]=='e')
            extract();
    }
    return 0;
}
