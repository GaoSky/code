//
//  C12-二叉树II-超级备忘录[Splay].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/6/12.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 150005
#define inf 0x7fffffff
using namespace std;
struct Node
{
    int ch[2],val,cnt,size;
    int rev,add,fa,minn;
}a[maxn];
int tot,root,n,m;
void Modify_rev(int x)
{
    if(!x)
        return;
    swap(a[x].ch[0],a[x].ch[1]);
    a[x].rev^=1;
}
void Modify_add(int x,int val)
{
    if(x)
    {
        a[x].add+=val;
        a[x].val+=val;
        a[x].minn+=val;
    }
}
void Create(int x,int val,int fa)
{
    a[x].fa=fa,a[x].size=1;
    a[x].val=a[x].minn=val;
    a[x].ch[0]=a[x].ch[1]=a[x].rev=a[x].add=0;
}
void pushup(int x)
{
    if(!x)
        return;
    a[x].size=1;
    a[x].minn=a[x].val;
    if(a[x].ch[0])
    {
        a[x].size+=a[a[x].ch[0]].size;
        a[x].minn=min(a[x].minn,a[a[x].ch[0]].minn);
    }
    if(a[x].ch[1])
    {
        a[x].size+=a[a[x].ch[1]].size;
        a[x].minn=min(a[x].minn,a[a[x].ch[1]].minn);
    }
}
void Release(int x)
{
    if(!x)
        return;
    if(a[x].add)
    {
        Modify_add(a[x].ch[0],a[x].add);
        Modify_add(a[x].ch[1],a[x].add);
        a[x].add=0;
    }
    if(a[x].rev)
    {
        Modify_rev(a[x].ch[0]);
        Modify_rev(a[x].ch[1]);
        a[x].rev=0;
    }
}
void build(int &x,int l,int r,int fa)
{
    if(l>r)
        return;
    int mid=(l+r)>>1;
    x=mid;
    Create(x,a[x].val,fa);
    build(a[x].ch[0],l,mid-1,x);
    build(a[x].ch[1],mid+1,r,x);
    pushup(x);
}
void Rotate(int x,int type)
{
    int y=a[x].fa,z=a[y].fa;
    Release(y),Release(x);
    a[y].ch[!type]=a[x].ch[type];
    if(a[x].ch[type])
        a[a[x].ch[type]].fa=y;
    a[x].fa=z;
    if(z)
        a[z].ch[a[z].ch[1]==y]=x;
    a[y].fa=x;
    a[x].ch[type]=y;
    pushup(y),pushup(x);
}
void Splay(int x,int goal)
{
    Release(x);
    while(a[x].fa!=goal)
    {
        int y=a[x].fa,z=a[y].fa;
        Release(z),Release(y),Release(x);
        if(a[y].fa==goal)
            Rotate(x,a[y].ch[0]==x);
        else
        {
            int p=(a[a[y].fa].ch[0]==y);
            if(a[y].ch[p]==x)
                Rotate(x,!p),Rotate(x,p);
            else
                Rotate(y,p),Rotate(x,p);
        }
    }
    pushup(x);
    if(goal==0)
        root=x;
}
int Query(int x,int rank)
{
    Release(x);
    if(a[a[x].ch[0]].size+1==rank)
        return x;
    else if(a[a[x].ch[0]].size>=rank)
        return Query(a[x].ch[0],rank);
    else
        return Query(a[x].ch[1],rank-a[a[x].ch[0]].size-1);
}
void Exchange(int l1,int r1,int l2,int r2)
{
    int x=Query(root,l2-1),y=Query(root,r2+1);
    Splay(x,0),Splay(y,x);
    int right=a[y].ch[0];
    a[y].ch[0]=0;
    x=Query(root,l1-1),y=Query(root,l1);
    Splay(x,0),Splay(y,x);
    a[y].ch[0]=right;
    a[right].fa=y;
}
void Reverse(int l,int r)
{
    int x=Query(root,l-1),y=Query(root,r+1);
    Splay(x,0),Splay(y,x);
    Modify_rev(a[y].ch[0]);
}
void Add(int l,int r,int val)
{
    int x=Query(root,l-1),y=Query(root,r+1);
    Splay(x,0),Splay(y,x);
    Modify_add(a[y].ch[0],val);
}
void Insert(int now,int val)
{
    int r=Query(root,now),rr=Query(root,now+1);
    Splay(r,0),Splay(rr,r);
    Create(++tot,val,rr);
    a[rr].ch[0]=tot;
    for(r=rr;r;r=a[r].fa)
        Release(r),pushup(r);
    Splay(rr,0);
}
void _Delete(int x)
{
    int tmp1=Query(root,x-1),tmp2=Query(root,x+1);
    Splay(tmp1,0),Splay(tmp2,root);
    a[a[root].ch[1]].ch[0]=0;
    pushup(a[root].ch[1]),pushup(root);
}
int get_min(int l,int r)
{
    int x=Query(root,l-1);
    int y=Query(root,r+1);
    Splay(x,0),Splay(y,x);
    return a[a[y].ch[0]].minn;
}
int main()
{
    scanf("%d",&n);
    a[1].val=a[n+2].val=inf;
    for(re int i=2;i<=n+1;++i)
        scanf("%d",&a[i].val);
    tot=n+2;
    a[root].fa=a[root].size=a[root].ch[0]=a[root].ch[1]=a[root].rev=a[root].add=0;
    build(root,1,tot,0);
    pushup(root);
    scanf("%d",&m);
    char op[20];
    while(m--)
    {
        int l,r,val;
        scanf("%s",op);
        if(op[0]=='A')
        {
            scanf("%d%d%d",&l,&r,&val);
            Add(l+1,r+1,val);
        }
        else if(op[0]=='I')
        {
            scanf("%d%d",&l,&val);
            Insert(l+1,val);
        }
        else if(op[0]=='M')
        {
            scanf("%d%d",&l,&r);
            printf("%d\n",get_min(l+1,r+1));
        }
        else if(op[0]=='D')
        {
            scanf("%d",&val);
            _Delete(val+1);
        }
        else if(op[3]=='E')
        {
            scanf("%d%d",&l,&r);
            Reverse(l+1,r+1);
        }
        else
        {
            scanf("%d%d%d",&l,&r,&val);
            val=val%(r-l+1);
            if(val)
                Exchange(l+1,r-val+1,r-val+1+1,r+1);
        }
    }
    return 0;
}
