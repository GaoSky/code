//
//  C12-二叉树II-二叉搜索树删除.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/27.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 500005
using namespace std;
struct Node
{
    int left,right,parent,val;
}a[maxn];
int cnt,root;
void insert(int k)
{
    int y=0,x=root;
    while(x!=0)
    {
        y=x;
        if(a[k].val>a[x].val)
            x=a[x].right;
        else
            x=a[x].left;
    }
    a[k].parent=y;
    if(y==0)
        root=k;
    else if(a[k].val<a[y].val)
        a[y].left=k;
    else
        a[y].right=k;
}
void print1(int k)
{
    if(k==0)
        return;
    printf(" %d",a[k].val);
    print1(a[k].left);
    print1(a[k].right);
}
void print2(int k)
{
    if(k==0)
        return;
    print2(a[k].left);
    printf(" %d",a[k].val);
    print2(a[k].right);
}
int find(int v,int k)
{
    if(k==0)
        return 0;
    if(a[k].val==v)
        return k;
    if(a[k].val>v)
        return find(v,a[k].left);
    else
        return find(v,a[k].right);
}
void _Delete(int k)
{
    int n;
    if(k==a[a[k].parent].left)
        n=0;
    else
        n=1;
    if(!a[k].left&&!a[k].right)
    {
        if(n==0)
            a[a[k].parent].left=0;
        else
            a[a[k].parent].right=0;
    }
    else if(!a[k].left&&a[k].right)
    {
        if(n==0)
            a[a[k].parent].left=a[k].right;
        else
            a[a[k].parent].right=a[k].right;
        a[a[k].right].parent=a[k].parent;
    }
    else if(a[k].left&&!a[k].right)
    {
        if(n==0)
            a[a[k].parent].left=a[k].left;
        else
            a[a[k].parent].right=a[k].left;
        a[a[k].left].parent=a[k].parent;
    }
    else if(a[k].left&&a[k].right)
    {
        int tmp=a[k].right;
        while(a[tmp].left!=0)
            tmp=a[tmp].left;
        _Delete(tmp);
        a[k].val=a[tmp].val;
    }
}
int main()
{
    int t,x,kase=0;char op[10];
    scanf("%d",&t);
    while(t--)
    {
        scanf("%s",op);
        if(op[0]=='i')
        {
            scanf("%d",&x);
            a[++kase].val=x;
            insert(kase);
            cnt++;
        }
        else if(op[0]=='f')
        {
            scanf("%d",&x);
            int tmp=find(x,root);
            if(tmp<1||tmp>cnt)
                printf("no\n");
            else
                printf("yes\n");
        }
        else if(op[0]=='p')
        {
            print2(root),putchar('\n');
            print1(root),putchar('\n');
        }
        else if(op[0]=='d')
        {
            scanf("%d",&x);
            _Delete(find(x,root));
        }
    }
    return 0;
}
