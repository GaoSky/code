//
//  C08-并查集-整数序列[带权并查集].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/11.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
int fa[maxn],d[maxn],size[maxn];
int n,q;
int find(int x)
{
    if(fa[x]==x)
        return x;
    int root=find(fa[x]);
    d[x]+=d[fa[x]];
    return fa[x]=root;
}
void merge(int x,int y,int z)
{
    int px=find(x),py=find(y);
    if(px!=py)
    {
        fa[px]=py;
        d[px]=-d[x]+d[y]+z;
    }
}
int main()
{
    scanf("%d%d",&n,&q);
    for(re int i=1;i<=n;++i)
        fa[i]=i,size[i]=0,d[i]=0;
    int op,x,y,z;
    while(q--)
    {
        scanf("%d",&op);
        if(op==0)
        {
            scanf("%d%d%d",&x,&y,&z);
            merge(x,y,z);
        }
        else
        {
            scanf("%d%d",&x,&y);
            if(find(x)==find(y))
                printf("%d\n",d[x]-d[y]);
            else
                printf("?\n");
        }
    }
    return 0;
}
