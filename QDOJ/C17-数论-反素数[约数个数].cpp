//
//  C17-数论-反素数[约数个数].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/13.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define ull unsigned long long
using namespace std;
const int prime[16]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53};
ull ans,n;
int maxx;
inline void dfs(int depth,ull now,int num)
{
    if(depth>=16)
        return;
    if(num>maxx)
        maxx=num,ans=now;
    if(num==maxx&&ans>now)
        ans=now;
    for(re int i=1;i<=63;++i)
    {
        if(prime[depth]*now>n)
            break;
        now=now*prime[depth];
        dfs(depth+1,now,num*(i+1));
    }
}
int main()
{
    scanf("%llu",&n);
    ans=0xffffffffffffffff,maxx=0;
    dfs(0,1,1);
    printf("%llu",ans);
    return 0;
}
