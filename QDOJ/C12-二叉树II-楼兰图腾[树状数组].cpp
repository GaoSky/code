//
//  C12-二叉树II-楼兰图腾[树状数组].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/11.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define lowbit(x) x&(-x)
#define maxn 200005
using namespace std;
long long r[maxn],l[maxn],t[maxn],a[maxn];
int n;
long long ans;
void add(int x,int val)
{
    for(;x<=n;x+=lowbit(x))
        t[x]+=val;
}
int query(int x)
{
    int res=0;
    for(;x;x-=lowbit(x))
        res+=t[x];
    return res;
}
int main()
{
    long long sum=0;
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
        scanf("%lld",&a[i]),sum=max(sum,a[i]);
    for(re int i=n;i>=1;--i)
        r[i]+=query(sum)-query(a[i]),add(a[i],1);
    memset(t,0,sizeof(t));
    for(re int i=1;i<=n;++i)
        l[i]+=query(sum)-query(a[i]),add(a[i],1);
    for(re int i=1;i<=n;++i)
        ans+=l[i]*r[i];
    printf("%lld ",ans);
    memset(l,0,sizeof(l));
    memset(r,0,sizeof(r));
    memset(t,0,sizeof(t));
    for(re int i=n;i>=1;--i)
        r[i]+=query(a[i]-1),add(a[i],1);
    memset(t,0,sizeof(t));
    for(re int i=1;i<=n;++i)
        l[i]+=query(a[i]-1),add(a[i],1);
    ans=0;
    for(re int i=1;i<=n;++i)
        ans+=l[i]*r[i];
    printf("%lld",ans);
    return 0;
}
