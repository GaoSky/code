//
//  C12-二叉树II-郁闷的出纳员[Splay].cpp
//  Algorithm
//
//  Created by SkyGao on 2020/6/6.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
#define inf 0x7fffffff
using namespace std;
struct Node
{
    int l,r,val,cnt,size;
}a[maxn];
int tot,root,tag[maxn];
int minn,n,leave;
inline void Update(int x)
{
    a[x].size=a[a[x].l].size+a[a[x].r].size+a[x].cnt;
}
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
    Update(a[x].r),Update(x);
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
    Update(a[x].l),Update(x);
}
void Create(int &now,int val)
{
    now=++tot;
    a[tot].val=val;
    a[tot].cnt++;
    a[tot].size++;
}
void Splay(int x,int &y)
{
    if(x==y)
        return;
    int &l=a[y].l,&r=a[y].r;
    if(x==l)
        zig(y);
    else if(x==r)
        zag(y);
    else
    {
        if(a[x].val<a[y].val)
        {
            if(a[x].val<a[l].val)
                Splay(x,a[l].l),zig(y),zig(y);
            else
                Splay(x,a[l].r),zag(l),zig(y);
        }
        else
        {
            if(a[x].val>a[r].val)
                Splay(x,a[r].r),zag(y),zag(y);
            else
                Splay(x,a[r].l),zig(r),zag(y);
        }
    }
}
void insert(int &now,int &val)
{
    if(now==0)
    {
        Create(now,val);
        Splay(now,root);
        return;
    }
    else if(val<a[now].val)
        insert(a[now].l,val);
    else if(val>a[now].val)
        insert(a[now].r,val);
    else
    {
        a[now].cnt++;
        a[now].size++;
        Splay(now,root);
    }
}
void pushdown(int x)
{
    if(tag[x]!=0)
    {
        int l=a[x].l,r=a[x].r;
        if(l==0&&r==0)
        {
            a[x].val+=tag[x],tag[x]=0;
            return;
        }
        if(l!=0)
            tag[l]+=tag[x];
        if(r!=0)
            tag[r]+=tag[x];
        a[x].val+=tag[x];
        tag[x]=0;
        return;
    }
}
int Query2(int rank)
{
    int now=root;
    while(now)
    {
        pushdown(now);
        int rsize=a[a[now].r].size;
        if(rsize+1<=rank&&rank<=rsize+a[now].cnt)
        {
            Splay(now,root);
            break;
        }
        else if(rsize>=rank)
            now=a[now].r;
        else
            rank=rank-a[a[now].r].size-a[now].cnt,now=a[now].l;
    }
    return a[now].val;
}
void Add(int x)
{
    pushdown(root);
    tag[root]=x;
}
void fire()
{
    int x=root,y=-1;
    while(x)
    {
        pushdown(x);
        if(a[x].val<minn)
            x=a[x].r;
        else
        {
            y=x;
            x=a[x].l;
        }
    }
    if(y==-1)
    {
        leave+=a[root].size;
        root=0;
        return;
    }
    Splay(y,root);
    leave+=a[a[root].l].size;
    a[root].l=0;
    Update(root);
}
int main()
{
    char op[10];int x;
    scanf("%d%d",&n,&minn);
    for(re int i=1;i<=n;++i)
    {
        scanf("%s",op);scanf("%d",&x);
        if(op[0]=='I')
        {
            if(x>=minn)
                insert(root,x);
        }
        else if(op[0]=='A')
            Add(x);
        else if(op[0]=='S')
            Add(-x),fire();
        else if(op[0]=='F')
        {
            if(x>a[root].size)
                printf("-1\n");
            else
                printf("%d\n",Query2(x));
        }
    }
    printf("%d",leave);
    return 0;
}
