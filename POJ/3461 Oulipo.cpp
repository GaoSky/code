//
//  C22-字符串II-Oulipo.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/4.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
using namespace std;
char a[10005],b[1000005];
int nxt[10005],f[1000005];
int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
    {
        int ans=0;
        scanf("%s%s",a+1,b+1);
        int n=strlen(a+1),m=strlen(b+1);
        nxt[1]=0;
        for(re int i=2,j=0;i<=n;++i)
        {
            while(j>0&&a[j+1]!=a[i])
                j=nxt[j];
            if(a[j+1]==a[i])
                j++;
            nxt[i]=j;
        }
        for(re int i=1,j=0;i<=m;++i)
        {
            while(j>0&&(j==n||a[j+1]!=b[i]))
                j=nxt[j];
            if(a[j+1]==b[i])
                j++;
            f[i]=j;
            if(f[i]==n)
                ans++;
        }
        printf("%d\n",ans);
    }
    return 0;
}
