//
//  尺取法-Jessica's Reading Problem.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/4.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <set>
#include <map>
#define re register
#define maxn 1000005
using namespace std;
int n,a[maxn],p;
int main()
{
    set<int> se;
    scanf("%d",&p);
    for(re int i=1;i<=p;++i)
        scanf("%d",&a[i]),se.insert(a[i]);
    n=se.size();
    int s=1,t=1,num=0,res=p+1;
    map<int,int> m;
    while(true)
    {
        while(t<=p&&num<n)
        {
            if(m[a[t]]==0)
                num++;
            m[a[t]]++,t++;
        }
        if(num<n)
            break;
        res=min(res,t-s);
        if(--m[a[s]]==0)
            num--;
        s++;
    }
    printf("%d",res);
    return 0;
}
