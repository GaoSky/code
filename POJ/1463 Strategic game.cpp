//
//  1463 Strategic game.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/28.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#define re register
#define maxn 1505
using namespace std;
vector<int> son[maxn];
int n,dp[maxn][2],fa[maxn];
void Solve(int x)
{
    if(son[x].empty())
    {
        dp[x][0]=0,dp[x][1]=1;
        return;
    }
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        Solve(y);
        dp[x][0]+=dp[y][1];
        dp[x][1]+=min(dp[y][0],dp[y][1]);
    }
    dp[x][1]++;
}
int main()
{
    int num,cnt,x,root=-1;
    while(scanf("%d",&n)!=EOF)
    {
        memset(fa,-1,sizeof(fa));
        memset(dp,0,sizeof(dp));
        for(re int i=1;i<=n;++i)
        {
            scanf("%d:(%d)",&num,&cnt);
            for(re int i=1;i<=cnt;++i)
            {
                scanf("%d",&x);
                fa[x]=num;
                son[num].push_back(x);
            }
        }
        for(re int i=0;i<n;++i)
            if(fa[i]==-1)
                root=i;
        Solve(root);
        printf("%d\n",min(dp[root][0],dp[root][1]));
        for(re int i=0;i<n;++i)
            son[i].clear();
    }
    return 0;
}
