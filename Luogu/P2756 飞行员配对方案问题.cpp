//
//  P2756 飞行员配对方案问题.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/31.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#define re register
#define inf 0x7fffffff
#define maxn 105
using namespace std;
int n,m,s,t;
long long ans;int d[maxn];
struct Edge
{
    int to,val,rev;
    Edge(int x,int y,int z)
    {
        to=x;val=y;rev=z;
    }
};
vector<Edge> son[maxn];
inline void addedge(int x,int y,int z)
{
    son[x].push_back(Edge(y,z,son[y].size()));
    son[y].push_back(Edge(x,0,son[x].size()-1));
}
inline bool bfs()
{
    memset(d,-1,sizeof(d));
    queue<int> q;
    q.push(s);
    d[s]=0;
    while(!q.empty())
    {
        int x=q.front();
        q.pop();
        for(re int i=0;i<son[x].size();++i)
        {
            int y=son[x][i].to;
            if(d[y]==-1&&son[x][i].val>0)
            {
                q.push(y);
                d[y]=d[x]+1;
            }
        }
    }
    if(d[t]==-1)
        return false;
    else
        return true;
}
inline int dfs(int x,int low)
{
    if(x==t||low==0)
        return low;
    int totflow=0;
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i].to;
        int rev=son[x][i].rev;
        if(d[y]==d[x]+1&&son[x][i].val>0)
        {
            int minval=dfs(y,min(low,son[x][i].val));
            son[x][i].val-=minval;
            son[y][rev].val+=minval;
            low-=minval;
            totflow+=minval;
            if(low==0)
                return totflow;
        }
    }
    if(low!=0)
        d[x]=-1;
    return totflow;
}
void dinic()
{
    while(bfs())
        ans+=dfs(s,inf);
}
int main()
{
    scanf("%d%d",&m,&n);
    s=0,t=n+1;
    for(re int i=1;i<=m;++i)
        addedge(s,i,1);
    for(re int i=m+1;i<=n;++i)
        addedge(i,t,1);
    int x,y;
    scanf("%d%d",&x,&y);
    if(x==-1&&y==-1)
    {
        printf("0");
        return 0;
    }
    while(x!=-1&&y!=-1)
    {
        addedge(x,y,0x7fffffff);
        scanf("%d%d",&x,&y);
    }
    dinic();
    printf("%lld\n",ans);
    for(re int i=1;i<=m;++i)
        for(re int j=0;j<son[i].size();++j)
        {
            int rev=son[i][j].rev;
            int y=son[i][j].to;
            if(y==s||son[y][rev].to==s)
                continue;
            if(y==t||son[y][rev].to==t)
                continue;
            if(son[y][rev].val!=0)
                printf("%d %d\n",son[y][rev].to,y);
        }
    return 0;
}
