//
//  P2312 解方程.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/24.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>
#define re register
#define mod 1000000007
using namespace std;
bool flag=true;
int n,m,cnt;
int solution[1000005],a[105];
long long read()
{
    long long res=0,op=1;
    char c=getchar();
    while(c<'0'||c>'9')
    {
        if(c=='-')
            op=-1;
        c=getchar();
    }
    while(c>='0'&&c<='9')
    {
        res=(res*10+c-'0')%mod;
        c=getchar();
    }
    return res*op;
}
bool judge(long long x)
{
    int res=0;
    for(re int i=n;i>0;--i)
        res=((res+a[i])*x)%mod;
    res=(res+a[0])%mod;
    return !res;
}
int main()
{
    n=read(),m=read();
    for(re int i=0;i<=n;++i)
        a[i]=read();
    for(re int i=1;i<=m;++i)
        if(judge(i))
            solution[++cnt]=i;
    printf("%d\n",cnt);
    for(re int i=1;i<=cnt;++i)
        printf("%d\n",solution[i]);
    return 0;
}
