//
//  P5690 [CSP-SJX2019]日期.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
using namespace std;
const int days[15]={0,31,28,31,30,31,30,31,31,30,31,30,31};
int main()
{
    int m,d;
    scanf("%d-%d",&m,&d);
    if(m>=1&&m<=12)
    {
        if(d>days[m])
            printf("1");
        else
            printf("0");
    }
    else if(m!=0)
    {
        if(m/10==1&&d<=31)
            printf("1");
        else if(m%10>=0&&m%10<=2)
        {
            if((days[m%10]>=d||days[m%10+10]>=d)&&d!=0)
                printf("1");
            else
                printf("2");
        }
        else
        {
            if(days[m%10]>=d&&d!=0)
                printf("1");
            else
                printf("2");
        }
    }
    else
    {
        if(d>31||d==0)
            printf("2");
        else
            printf("1");
    }
    return 0;
}
