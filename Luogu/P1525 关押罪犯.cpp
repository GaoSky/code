//
//  P1525 关押罪犯.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/11.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
struct Person
{
    int x,y,val;
}a[maxn];
int n,m,fa[maxn*2];
long long ans;
bool cmp(Person x,Person y)
{
    return x.val>y.val;
}
int find(int x)
{
    return fa[x]=(fa[x]==x)?x:find(fa[x]);
}
void merge(int x,int y)
{
    x=find(x),y=find(y);
    if(x!=y)
        fa[x]=y;
}
int main()
{
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n*2;++i)
        fa[i]=i;
    for(re int i=1;i<=m;++i)
        scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].val);
    sort(a+1,a+m+1,cmp);
    for(re int i=1;i<=m;++i)
    {
        if(find(a[i].x)==find(a[i].y)||find(a[i].x+n)==find(a[i].y+n))
        {
            ans=a[i].val;
            break;
        }
        merge(a[i].x,a[i].y+n);
        merge(a[i].x+n,a[i].y);
    }
    printf("%lld",ans);
    return 0;
}
