//
//  P2384 最短路.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/2.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#define re register
#define maxn 1005
#define mod 9987
using namespace std;
int n,m,s;
long long dis[maxn];
vector<int> son[maxn];
vector<long long> v[maxn];
bool vis[maxn];
void addedge(int x,int y,long long z)
{
    son[x].push_back(y);
    if(z==0)
        z=mod-1;
    v[x].push_back(z);
}
struct Node
{
    int x;
    long long dis;
    bool operator < (const Node &a)const
    {
        return dis>a.dis;
    }
};priority_queue<Node> q;
void Dijkstra(int S)
{
    for(re int i=1;i<=n;++i)
        dis[i]=mod-1;
    dis[S]=1,q.push((Node){S,0});
    while(!q.empty())
    {
        Node prt=q.top();q.pop();
        int rt=prt.x;
        if(vis[rt])
            continue;
        vis[rt]=true;
        for(re int i=0;i<son[rt].size();++i)
        {
            int to=son[rt][i];
            if(dis[to]%mod>(dis[rt]*v[rt][i])%mod)
            {
                dis[to]=(dis[rt]*v[rt][i])%mod;
                q.push((Node){to,dis[to]});
            }
        }
    }
}
int main()
{
    scanf("%d%d",&n,&m);
    int x,y;
    long long z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%lld",&x,&y,&z);
        addedge(x,y,z%mod);
    }
    Dijkstra(1);
    printf("%lld",dis[n]);
    return 0;
}
