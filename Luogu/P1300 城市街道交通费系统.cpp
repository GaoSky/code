//
//  P1300 城市街道交通费系统.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/9/16.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <queue>
#define re register
#define maxn 35
using namespace std;
const int nxt[4][2]={{-1,0},{0,1},{1,0},{0,-1}};
int step[maxn][maxn][4],px,py,tx,ty,sx,sy,direct;
int n,m;
bool mp[maxn][maxn];
char op[maxn];
struct Node
{
    int x,y,to;
    bool operator < (const Node &a)const
    {
        return step[a.x][a.y][a.to]<step[x][y][to];
    }
}tmp,prt;
priority_queue<Node> q;
bool check(int x,int y)
{
    if(x>=1&&y>=1&&x<=n&&y<=m)
        return true;
    return false;
}
int main()
{
    memset(step,0x7f,sizeof(step));
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
    {
        scanf("%s",op+1);
        for(re int j=1;j<=m;++j)
        {
            if(op[j]!='#')
            {
                if(op[j]=='.')
                    mp[i][j]=true;
                else if(op[j]!='F')
                {
                    switch(op[j])
                    {
                        case 'N': tmp.to=0;break;
                        case 'E': tmp.to=1;break;
                        case 'S': tmp.to=2;break;
                        case 'W': tmp.to=3;break;
                    }
                    tmp.x=i,tmp.y=j;
                }
                else
                    px=i,py=j;
            }
        }
    }
    step[tmp.x][tmp.y][tmp.to]=0;
    q.push(tmp);
    bool flag=false;
    while(!q.empty())
    {
        flag=false;
        prt=q.top();q.pop();
        sx=prt.x,sy=prt.y;
        if(prt.x==px&&prt.y==py)
            break;
        tx=sx+nxt[prt.to][0];
        ty=sy+nxt[prt.to][1];
        if(!mp[tx][ty]) flag=true;
        if(!mp[tx][ty]&&step[tx][ty][prt.to]>step[sx][sy][prt.to]&&check(tx,ty))
        {
            step[tx][ty][prt.to]=step[sx][sy][prt.to];
            q.push((Node){tx,ty,prt.to});
        }
        direct=(prt.to+3)%4;
        tx=sx+nxt[direct][0];
        ty=sy+nxt[direct][1];
        if(!mp[tx][ty]) flag=true;
        if(!mp[tx][ty]&&step[tx][ty][direct]>step[sx][sy][prt.to]+1&&check(tx,ty))
        {
            step[tx][ty][direct]=step[sx][sy][prt.to]+1;
            q.push((Node){tx,ty,direct});
        }
        direct=(prt.to+1)%4;
        tx=sx+nxt[direct][0];
        ty=sy+nxt[direct][1];
        if(!mp[tx][ty]) flag=true;
        if(!mp[tx][ty]&&step[tx][ty][direct]>step[sx][sy][prt.to]+5&&check(tx,ty))
        {
            step[tx][ty][direct]=step[sx][sy][prt.to]+5;
            q.push((Node){tx,ty,direct});
        }
        if(!flag)
        {
            direct=(prt.to+2)%4;
            tx=sx+nxt[direct][0];
            ty=sy+nxt[direct][1];
            if(!mp[tx][ty]&&step[tx][ty][direct]>step[sx][sy][prt.to]+10&&check(tx,ty))
            {
                step[tx][ty][direct]=step[sx][sy][prt.to]+10;
                q.push((Node){tx,ty,direct});
            }
        }
    }
    printf("%d",step[sx][sy][prt.to]);
    return 0;
}
