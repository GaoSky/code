//
//  P1072 Hankson 的趣味题.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/24.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>
#include <cmath>
#define re register
using namespace std;
inline int gcd(int a,int b)
{
    if(b==0)
        return a;
    return gcd(b,a%b);
}
int main()
{
    int t,ans=0;
    int a0,a1,b0,b1;
    scanf("%d",&t);
    while(t--)
    {
        ans=0;
        scanf("%d%d%d%d",&a0,&a1,&b0,&b1);
        for(re int i=1;i<=sqrt(b1);++i)
            if(b1%i==0)
            {
                if(i%a1==0)
                    if(gcd(i/a1,a0/a1)==1&&gcd(b1/b0,b1/i)==1)
                        ans++;
                int x=b1/i;
                if(x==i)
                    continue;
                if(x%a1==0)
                    if(gcd(x/a1,a0/a1)==1&&gcd(b1/b0,b1/x)==1)
                        ans++;
            }
        printf("%d\n",ans);
    }
    return 0;
}
