//
//  P1373 小a和uim之大逃离.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/2.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#define re register
#define mod 1000000007
using namespace std;
int n,m,k;
int f[801][801][17][2];
int mp[801][801];
int main()
{
    scanf("%d%d%d",&n,&m,&k),k++;
    for(re int i=1;i<=n;++i)
        for(re int j=1;j<=m;++j)
        {
            scanf("%d",&mp[i][j]);
            f[i][j][mp[i][j]%k][0]=1;
        }
    for(re int i=1;i<=n;++i)
        for(re int j=1;j<=m;++j)
            for(re int p=0;p<=k;++p)
            {
                f[i][j][p][0]=(f[i][j][p][0]+f[i-1][j][(p-mp[i][j]+k)%k][1])%mod;
                f[i][j][p][0]=(f[i][j][p][0]+f[i][j-1][(p-mp[i][j]+k)%k][1])%mod;
                f[i][j][p][1]=(f[i][j][p][1]+f[i-1][j][(p+mp[i][j])%k][0])%mod;
                f[i][j][p][1]=(f[i][j][p][1]+f[i][j-1][(p+mp[i][j])%k][0])%mod;
            }
    long long ans=0;
    for(re int i=1;i<=n;++i)
        for(re int j=1;j<=m;++j)
            ans=(ans+f[i][j][0][1])%mod;
    printf("%lld",ans);
    return 0;
}
