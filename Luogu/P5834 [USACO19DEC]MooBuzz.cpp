//
//  P5834 [USACO19DEC]MooBuzz.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#define re register
using namespace std;
int main()
{
    int n;
    scanf("%d",&n);
    long long ans=(long long)n*15/8;
    while(ans%3==0||ans%5==0)
        ans--;
    printf("%lld",ans);
    return 0;
}
