//
//  P1204 [USACO1.2]挤牛奶Milking Cows.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/31.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#define re register
#define maxn 5005
using namespace std;
int n,ans1=0,ans2=0;
struct Timeline
{
    int l,r;
}a[maxn];
inline int read()
{
    int res=0,op=1;char c=getchar();
    while(c<'0'||c>'9')
    {
        if(c=='-') op=-1;
        c=getchar();
    }
    while(c>='0'&&c<='9')
        res=res*10+c-'0',c=getchar();
    return res*op;
}
inline bool cmp(Timeline a,Timeline b)
{
    return a.l<b.l;
}
int main()
{
    n=read();
    for(re int i=1;i<=n;++i)
        a[i].l=read(),a[i].r=read();
    sort(a+1,a+n+1,cmp);
    int begin=a[1].l,end=a[1].r;
    for(re int i=2;i<=n;++i)
    {
        if(a[i].l<=end)
            end=max(end,a[i].r);
        else
        {
            ans1=max(ans1,end-begin);
            ans2=max(ans2,a[i].l-end);
            begin=a[i].l;
            end=a[i].r;
        }
    }
    ans1=max(ans1,end-begin);
    printf("%d %d",ans1,ans2);
    return 0;
}
