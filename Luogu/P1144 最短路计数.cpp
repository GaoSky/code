//
//  P1144 最短路计数.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/22.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#define re register
#define maxn 1000005
#define mod 100003
using namespace std;
int n,m,cnt[maxn],step[maxn];
bool vis[maxn];
vector<int> son[maxn];
queue<int> q;
int main()
{
    int x,y;
    scanf("%d%d",&n,&m);
    step[1]=0,vis[1]=true,cnt[1]=1;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d",&x,&y);
        son[x].push_back(y);
        son[y].push_back(x);
    }
    q.push(1);
    while(!q.empty())
    {
        int x=q.front();q.pop();
        for(re int i=0;i<son[x].size();++i)
        {
            int y=son[x][i];
            if(!vis[y])
            {
                vis[y]=1,step[y]=step[x]+1;
                q.push(y);
            }
            if(step[y]==step[x]+1)
                cnt[y]=(cnt[y]+cnt[x])%mod;
        }
    }
    for(re int i=1;i<=n;++i)
        printf("%d\n",cnt[i]);
    return 0;
}
