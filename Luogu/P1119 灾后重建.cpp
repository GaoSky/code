//
//  P1119 灾后重建.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#define re register
#define maxn 205
using namespace std;
int n,m,Q,t[maxn];
int f[maxn][maxn];
void update(int k)
{
    for(re int i=0;i<n;++i)
        for(re int j=0;j<n;++j)
            if(f[i][j]>f[i][k]+f[k][j])
                f[i][j]=f[j][i]=f[i][k]+f[k][j];
}
int main()
{
    scanf("%d%d",&n,&m);
    for(re int i=0;i<n;++i)
        scanf("%d",&t[i]);
    for(re int i=0;i<n;++i)
        for(re int j=0;j<n;++j)
            f[i][j]=1e9;
    for(re int i=0;i<n;++i)
        f[i][i]=0;
    int x,y,z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        f[x][y]=f[y][x]=z;
    }
    scanf("%d",&Q);
    int now=0;
    while(Q--)
    {
        scanf("%d%d%d",&x,&y,&z);
        while(t[now]<=z&&now<n)
        {
            update(now);
            now++;
        }
        if(t[x]>z||t[y]>z)
        {
            printf("-1\n");
            continue;
        }
        if(f[x][y]==1e9)
        {
            printf("-1\n");
            continue;
        }
        printf("%d\n",f[x][y]);
    }
    return 0;
}
