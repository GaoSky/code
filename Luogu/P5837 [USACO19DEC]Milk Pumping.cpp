//
//  P5837 [USACO19DEC]Milk Pumping.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#define re register
#define maxn 1005
using namespace std;
int n,m;
int dis[maxn],vis[maxn],tim;
vector<int> son[maxn],v[maxn],flow[maxn];
void addedge(int x,int y,int z,int f)
{
    son[x].push_back(y);
    v[x].push_back(z);
    flow[x].push_back(f);
}
struct Node
{
    int x,dis;
    bool operator < (const Node &a)const
    {
        return dis>a.dis;
    }
};priority_queue<Node> q;
void Dijkstra(int S,int limit)
{
    for(re int i=1;i<=n;++i)
        dis[i]=0x7fffffff;
    dis[S]=0,q.push((Node){S,0}),tim++;
    while(!q.empty())
    {
        Node prt=q.top();q.pop();
        int rt=prt.x;
        if(vis[rt]==tim)
            continue;
        vis[rt]=tim;
        for(re int i=0;i<son[rt].size();++i)
        {
            if(flow[rt][i]<limit)
                continue;
            int to=son[rt][i];
            if(dis[to]>dis[rt]+v[rt][i])
            {
                dis[to]=dis[rt]+v[rt][i];
                q.push((Node){to,dis[to]});
            }
        }
    }
}
int main()
{
    int ans=0;
    scanf("%d%d",&n,&m);
    int x,y,z,w;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d%d",&x,&y,&z,&w);
        addedge(x,y,z,w);
        addedge(y,x,z,w);
    }
    for(re int i=1;i<=1000;++i)
    {
        Dijkstra(1,i);
        ans=max(ans,i*1000000/dis[n]);
    }
    printf("%d",ans);
    return 0;
}
