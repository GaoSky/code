//
//  P7071 优秀的拆分.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/10.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#define re register
using namespace std;
vector<int> ans;
int main()
{
    int n,cnt=0;
    scanf("%d",&n);
    if(n&1)
    {
        printf("-1");
        return 0;
    }
    while(n)
    {
        if(n&1)
            ans.push_back(cnt);
        cnt++,n>>=1;
    }
    for(re int i=ans.size()-1;i>=0;i--)
        printf("%d ",1<<ans[i]);
    return 0;
}
