//
//  P5832 [USACO19DEC]Where Am I?.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <map>
using namespace std;
string a;
string tmp;
char ch;
bool flag;
map<string,bool> mp;
int main()
{
    int n;
    scanf("%d",&n);
    ch=getchar();
    for(int i=1;i<=n;++i)
        ch=getchar(),a+=ch;
    for(int k=1;k<=n;++k)
    {
        flag=true;
        for(int i=1;i<=k;++i)
            tmp+=a[i-1];
        mp[tmp]=true;
        for(int i=k;i<n;++i)
        {
            tmp.erase(0,1),tmp+=a[i];
            if(mp[tmp]==false)
                mp[tmp]=true;
            else
            {
                flag=false;
                break;
            }
        }
        if(flag)
        {
            printf("%d",k);
            return 0;
        }
        tmp="",mp.clear();
    }
    return 0;
}
