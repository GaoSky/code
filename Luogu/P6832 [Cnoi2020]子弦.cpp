//
//  P6832 [Cnoi2020]子弦.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/10/12.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#define re register
#define maxn 10000005
using namespace std;
int cnt[27];
int main()
{
    char c;
    while((c=getchar())!=EOF)
        cnt[c-'a'+1]++;
    int ans=-1;
    for(re int i=1;i<=26;++i)
        ans=max(ans,cnt[i]);
    printf("%d",ans);
    return 0;
}
