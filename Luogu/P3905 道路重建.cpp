//
//  P3905 道路重建.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/2.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#define re register
#define maxn 105
using namespace std;
int n,m,s;
int dis[maxn];
int mp[maxn][maxn];
vector<int> son[maxn],v[maxn];
bool vis[maxn];
void addedge(int x,int y,int z)
{
    son[x].push_back(y);
    v[x].push_back(z);
}
struct Node
{
    int x,dis;
    bool operator < (const Node &a)const
    {
        return dis>a.dis;
    }
};priority_queue<Node> q;
void Dijkstra(int S)
{
    for(re int i=1;i<=n;++i)
        dis[i]=0x7fffffff;
    dis[S]=0,q.push((Node){S,0});
    while(!q.empty())
    {
        Node prt=q.top();q.pop();
        int rt=prt.x;
        if(vis[rt])
            continue;
        vis[rt]=true;
        for(re int i=0;i<son[rt].size();++i)
        {
            int to=son[rt][i];
            if(dis[to]>dis[rt]+v[rt][i])
            {
                dis[to]=dis[rt]+v[rt][i];
                q.push((Node){to,dis[to]});
            }
        }
    }
}
int main()
{
    scanf("%d%d",&n,&m);
    int x,y,z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        mp[x][y]=mp[y][x]=z;
        addedge(x,y,0);
        addedge(y,x,0);
    }
    scanf("%d",&s);
    for(re int i=1;i<=s;++i)
    {
        scanf("%d%d",&x,&y);
        for(re int i=0;i<son[x].size();++i)
            if(son[x][i]==y)
                v[x][i]=mp[x][y];
        for(re int i=0;i<son[y].size();++i)
            if(son[y][i]==x)
                v[y][i]=mp[y][x];
    }
    int begin,end;
    scanf("%d%d",&begin,&end);
    Dijkstra(begin);
    printf("%d",dis[end]);
    return 0;
}

