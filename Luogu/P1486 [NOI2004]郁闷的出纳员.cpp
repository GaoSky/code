//
//  P1486 [NOI2004]郁闷的出纳员.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/5/27.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
#define inf 0x7fffffff
using namespace std;
struct Treap
{
    int l,r,val,data,cnt,size;
}a[maxn];
int tot,root,n;
int minn,leave;
inline void Update(int x)
{
    a[x].size=a[a[x].l].size+a[a[x].r].size+a[x].cnt;
}
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
    Update(a[x].r),Update(x);
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
    Update(a[x].l),Update(x);
}
inline void Create(int val)
{
    a[++tot].val=val;
    a[tot].data=rand();
    a[tot].cnt=a[tot].size=1;
}
inline void init()
{
    Create(-inf),Create(inf);
    root=1;
    a[1].r=2;
    Update(root);
}
inline void insert(int &x,int val)
{
    if(x==0)
    {
        Create(val),x=tot;
        return;
    }
    if(val==a[x].val)
    {
        a[x].cnt++,Update(x);
        return;
    }
    else if(val<a[x].val)
    {
        insert(a[x].l,val);
        if(a[x].data<a[a[x].l].data)
            zig(x);
    }
    else
    {
        insert(a[x].r,val);
        if(a[x].data<a[a[x].r].data)
            zag(x);
    }
    Update(x);
}
inline void _Delete(int &x,int val)
{
    if(x==0)
        return;
    if(val==a[x].val)
    {
        if(a[x].l||a[x].r)
        {
            if(a[x].r==0||a[a[x].l].data>a[a[x].r].data)
                zig(x),_Delete(a[x].r,val);
            else
                zag(x),_Delete(a[x].l,val);
            Update(x);
        }
        else
            x=0;
        return;
    }
    if(val<a[x].val)
        _Delete(a[x].l,val);
    else
        _Delete(a[x].r,val);
    Update(x);
}
inline int Query2(int &x,int val)
{
    if(x==0)
        return inf;
    if(a[a[x].r].size>=val)
        return Query2(a[x].r,val);
    else if(a[a[x].r].size+a[x].cnt>=val)
        return a[x].val;
    return Query2(a[x].l,val-a[a[x].r].size-a[x].cnt);
}
inline void Add(int &p,int val)
{
    if(p==0)
        return;
    if(a[p].val!=inf&&a[p].val!=-inf)
        a[p].val+=val;
    Add(a[p].l,val),Add(a[p].r,val);
    if(p>2&&a[p].val<minn)
    {
        leave+=a[p].cnt;
        _Delete(root,a[p].val);
    }
}
int main()
{
    srand((unsigned)time(NULL));
    init();
    char op[10];int x;
    scanf("%d%d",&n,&minn);
    for(re int i=1;i<=n;++i)
    {
        scanf("%s",op);scanf("%d",&x);
        if(op[0]=='I')
        {
            if(x>=minn)
                insert(root,x);
        }
        else if(op[0]=='A')
            Add(root,x);
        else if(op[0]=='S')
            Add(root,-x);
        else if(op[0]=='F')
        {
            if(x>a[root].size-2)
                printf("-1\n");
            else
                printf("%d\n",Query2(root,x+1));
        }
    }
    printf("%d",leave);
    return 0;
}
