//
//  P1033 自由落体.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/25.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>
#define re register
using namespace std;
double h,s1,v,l,k;int n;
double tmin,tmax,numa,numb;
int ans;
int main()
{
    scanf("%lf%lf%lf%lf%lf%d",&h,&s1,&v,&l,&k,&n);
    tmin=sqrt(2.00*(h-k)/10.00);
    tmax=sqrt(2.00*h/10.00);
    int x1=s1+l-tmin*v,x2=s1-tmax*v;
    x1=min(x1,n),x2=max(0,x2);
    printf("%d",(int)(x1-x2));
    return 0;
}
