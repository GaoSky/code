//
//  P3381 【模板】最小费用最大流.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/31.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
struct Edge
{
    int to,val,rev,dis;
    Edge(int x,int y,int z,int w)
    {
        to=x;
        val=y;
        rev=z;
        dis=w;
    }
};vector<Edge> son[maxn];
int n,m,s,t,flow[maxn],dis[maxn],fa[maxn],pre[maxn];
bool vis[maxn];
int maxflow,maxcost;
queue<int> q;
inline void addedge(int u,int v,int w,int f)
{
    son[u].push_back(Edge(v,w,son[v].size(),f));
    son[v].push_back(Edge(u,0,son[u].size()-1,-f));
}
inline bool spfa()
{
    memset(dis,0x7f,sizeof(dis));
    memset(flow,0x7f,sizeof(flow));
    memset(vis,0,sizeof(vis));
    q.push(s);vis[s]=true;dis[s]=0;fa[t]=-1;
    while(!q.empty())
    {
        int prt=q.front();
        q.pop();
        vis[prt]=false;
        for(re int i=0;i<son[prt].size();++i)
        {
            Edge rt=son[prt][i];
            if(rt.val>0&&dis[rt.to]>dis[prt]+rt.dis)
            {
                dis[rt.to]=dis[prt]+rt.dis;
                fa[rt.to]=prt;
                pre[rt.to]=i;
                flow[rt.to]=min(flow[prt],rt.val);
                if(!vis[rt.to])
                {
                    vis[rt.to]=1;
                    q.push(rt.to);
                }
            }
        }
    }
    return fa[t]!=-1;
}
void Dinic()
{
    while(spfa())
    {
        maxflow+=flow[t];
        maxcost+=flow[t]*dis[t];
        for(re int now=t;now!=s;now=fa[now])
        {
            son[fa[now]][pre[now]].val-=flow[t];
            int rev=son[fa[now]][pre[now]].rev;
            son[now][rev].val+=flow[t];
        }
    }
}
int main()
{
    scanf("%d%d%d%d",&n,&m,&s,&t);
    int u,v,w,f;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d%d",&u,&v,&w,&f);
        addedge(u,v,w,f);
    }
    Dinic();
    printf("%d %d",maxflow,maxcost);
    return 0;
}
