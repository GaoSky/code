//
//  P2024 [NOI2001]食物链.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/11.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 150005
using namespace std;
int fa[maxn];
int n,ans,k;
int find(int x)
{
    return fa[x]=(fa[x]==x)?x:find(fa[x]);
}
void merge(int x,int y)
{
    fa[find(x)]=fa[find(y)];
}
int main()
{
    int x,y,z;
    scanf("%d%d",&n,&k);
    for(re int i=1;i<=3*n;++i)
        fa[i]=i;
    for(re int i=1;i<=k;++i)
    {
        scanf("%d%d%d",&z,&x,&y);
        if(x>n||y>n)
            ans++;
        else
        {
            if(z==1)
            {
                if(find(x)==find(y+n)||find(x)==find(y+n+n))
                    ans++;
                else
                    merge(x,y),merge(x+n,y+n),merge(x+n+n,y+n+n);
            }
            else if(z==2)
            {
                if(x==y||find(x)==find(y+n)||find(x)==find(y))
                    ans++;
                else
                {
                    merge(x,y+n+n);
                    merge(x+n,y);
                    merge(x+n+n,y+n);
                }
            }
        }
    }
    printf("%d",ans);
    return 0;
}
