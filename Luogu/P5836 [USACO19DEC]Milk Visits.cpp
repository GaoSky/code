//
//  P5836 [USACO19DEC]Milk Visits.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#define re register
#define maxn 100005
using namespace std;
int n,m;
int fa[maxn][20],depth[maxn];
int cnth[maxn],cntg[maxn];
vector<int> son[maxn];
char cow[maxn];
inline void addedge(int x,int y)
{
    son[x].push_back(y);
}
void dfs(int x,int S)
{
    cnth[x]=cnth[S]+(cow[x]=='H');
    cntg[x]=cntg[S]+(cow[x]=='G');
    depth[x]=depth[S]+1;
    fa[x][0]=S;
    for(re int i=0;i<son[x].size();++i)
    {
        int rt=son[x][i];
        if(rt==S)
            continue;
        dfs(rt,x);
    }
}
inline int go_up(int x,int d)
{
    for(re int j=19;j>=0;--j)
        if((1<<j)&d)
            x=fa[x][j];
    return x;
}
int main()
{
    scanf("%d%d",&n,&m);
    scanf("%s",cow+1);
    int x,y;
    for(re int i=1;i<n;++i)
    {
        scanf("%d%d",&x,&y);
        addedge(x,y),addedge(y,x);
    }
    dfs(1,1);
    for(re int j=1;j<=19;++j)
        for(re int i=1;i<=n;++i)
            fa[i][j]=fa[fa[i][j-1]][j-1];
    char op[10];
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d",&x,&y);
        int h=cnth[x]+cnth[y];
        int g=cntg[x]+cntg[y];
        scanf("%s",op);
        if(depth[x]>depth[y])
            x=go_up(x,depth[x]-depth[y]);
        if(depth[x]<depth[y])
            y=go_up(y,depth[y]-depth[x]);
        for(re int j=19;j>=0;--j)
            if(fa[x][j]!=fa[y][j])
                x=fa[x][j],y=fa[y][j];
        if(x!=y)
            x=fa[x][0];
        h=h-cnth[x]*2+(cow[x]=='H');
        g=g-cntg[x]*2+(cow[x]=='G');
        if(op[0]=='H')
        {
            if(h) putchar('1');
            else putchar('0');
        }
        else
        {
            if(g) putchar('1');
            else putchar('0');
        }
    }
    return 0;
}
