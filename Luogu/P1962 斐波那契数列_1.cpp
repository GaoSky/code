//
//  P1962 斐波那契数列_1.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/3/22.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
using namespace std;
namespace Matrix_Calculation
{
    long long mod=1000000007;
    int n=2;
    class Matrix
    {
    public:
        long long a[3][3];
        Matrix operator *(const Matrix&b)
        {
            Matrix res;
            for(re int i=1;i<=n;i++)
                for(re int j=1;j<=n;j++)
                {
                    res.a[i][j]=0;
                    for(re int p=1;p<=n;p++)
                    {
                        res.a[i][j]+=this->a[i][p]*b.a[p][j];
                        res.a[i][j]%=mod;
                    }
                }
            return res;
        }
        Matrix operator +(const Matrix&b)
        {
            Matrix res;
            for(re int i=1;i<=n;++i)
                for(re int j=1;j<=n;++j)
                    res.a[i][j]=this->a[i][j]+b.a[i][j];
            return res;
        }
        void init()
        {
            for(re int i=0;i<=n;++i)
                for(re int j=0;j<=n;++j)
                    a[i][j]=0;
        }
        Matrix unit()
        {
            Matrix ans;
            for(re int i=1;i<=n;i++)
                for(re int j=1;j<=n;j++)
                {
                    if(i==j)
                        ans.a[i][j]=1;
                    else
                        ans.a[i][j]=0;
                }
            return ans;
        }
        Matrix quickpow(Matrix a,long long b)
        {
            Matrix res;
            res=res.unit();
            while(b)
            {
                if(b&1)
                    res=res*a;
                a=a*a;
                b>>=1;
            }
            return res;
        }
    };
    ostream& operator<<(ostream &out,const Matrix& x)
    {
        for(re int i=1;i<=n;++i)
        {
            for(re int j=1;j<=n;++j)
                printf("%lld ",x.a[i][j]);
            printf("\n");
        }
        return out;
    }
}
using namespace Matrix_Calculation;
long long k;
int main()
{
    scanf("%lld",&k);
    if(k==1||k==2)
    {
        printf("1");
        return 0;
    }
    Matrix base,ans;
    base.init(),ans.init();
    base.a[1][1]=1,base.a[2][1]=1,base.a[1][2]=1;
    ans.a[1][1]=1,ans.a[1][2]=1;
    ans=ans*base.quickpow(base,k-2);
    printf("%lld",ans.a[1][1]%mod);
    return 0;
}
