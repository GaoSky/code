//
//  P2574 XOR的艺术.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/22.
//

#include <stdio.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#define re register
#define maxn 200005
using namespace std;
struct point
{
    int l,r,val,mark;
}tr[maxn*4];
int m,n,cnt,a,b,c,p[maxn];
int op;
void buildtree(int x,int l,int r)
{
    tr[x].l=l;
    tr[x].r=r;
    if(l==r)
    {
        tr[x].val=p[l];
        return;
    }
    int lch=x<<1,rch=x<<1|1;
    int mid=(l+r)>>1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
    tr[x].val=tr[lch].val+tr[rch].val;
}
void release(int x)
{
    if(tr[x].mark&&tr[x].l<tr[x].r)
    {
        int lch=x<<1,rch=x<<1|1;
        tr[lch].val=tr[lch].r-tr[lch].l+1-tr[lch].val;
        tr[rch].val=tr[rch].r-tr[rch].l+1-tr[rch].val;
        tr[lch].mark^=1;
        tr[rch].mark^=1;
    }
    tr[x].mark=0;
}
void modify(int x,int l,int r)
{
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
    {
        tr[x].val=tr[x].r-tr[x].l+1-tr[x].val;
        tr[x].mark^=1;
        return;
    }
    long long mid=(tr[x].l+tr[x].r)>>1;
    if(l<=mid)
        modify(x<<1,l,r);
    if(mid<r)
        modify(x<<1|1,l,r);
    tr[x].val=tr[x<<1].val+tr[x<<1|1].val;
}
int query(int x,int l,int r)
{
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
        return tr[x].val;
    int mid=(tr[x].l+tr[x].r)>>1;
    int ans=0;
    if(l<=mid)
        ans+=query(x*2,l,r);
    if(r>mid)
        ans+=query(x*2+1,l,r);
    return ans;
}
int main()
{
    scanf("%d%d",&n,&m);
    for(int i=1;i<=n;i++)
        scanf("%1d",&p[i]);
    buildtree(1,1,n);
    for(int i=1;i<=m;i++)
    {
        scanf("%d",&op);
        if(op==0)
        {
            scanf("%d%d",&a,&b);
            modify(1,a,b);
        }
        else
        {
            scanf("%d%d",&a,&b);
            printf("%d\n",query(1,a,b));
        }
    }
    return 0;
}
