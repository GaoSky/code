//
//  P1311 选择客栈.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/24.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#define re register
#define maxn 200005
using namespace std;
int n,k,p,ans,now,color,cost;
int cnt[maxn],sum[maxn],last[maxn];
int main()
{
    scanf("%d%d%d",&n,&k,&p);
    for(re int i=1;i<=n;++i)
    {
        scanf("%d%d",&color,&cost);
        if(cost<=p)
            now=i;
        if(now>=last[color])
            sum[color]=cnt[color];
        last[color]=i;
        ans+=sum[color];
        cnt[color]++;
    }
    printf("%d\n",ans);
    return 0;
}
