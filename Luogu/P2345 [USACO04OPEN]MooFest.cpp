//
//  P2345 [USACO04OPEN]MooFest.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/2.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#define re register
#define maxn 50005
#define lowbit(x) (x&(-x))
#define ll long long
using namespace std;
struct Cows
{
    ll voice,pos;
}c[maxn];
bool cmp(Cows a,Cows b)
{
    return a.voice<b.voice;
}
int n;ll c1[maxn],c2[maxn];
void modify1(ll x,ll value)
{
    while(x<maxn)
    {
        c1[x]+=value;
        x=x+lowbit(x);
    }
}
void modify2(ll x,ll value)
{
    while(x<maxn)
    {
        c2[x]+=value;
        x=x+lowbit(x);
    }
}
ll query1(ll x)
{
    ll ans=0;
    while(x)
    {
        ans+=c1[x];
        x=x-lowbit(x);
    }
    return ans;
}
ll query2(ll x)
{
    ll ans=0;
    while(x)
    {
        ans+=c2[x];
        x=x-lowbit(x);
    }
    return ans;
}
int main()
{
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
        scanf("%lld%lld",&c[i].voice,&c[i].pos);
    sort(c+1,c+n+1,cmp);
    ll ans=0,tot=0;
    for(re int i=1;i<=n;++i)
    {
        ll num=query1(c[i].pos);
        ll sum=query2(c[i].pos);
        ans+=(num*c[i].pos-sum)*c[i].voice;
        ans+=((tot-sum)-(i-num-1)*c[i].pos)*c[i].voice;
        modify1(c[i].pos,1);
        modify2(c[i].pos,c[i].pos);
        tot+=c[i].pos;
    }
    printf("%lld",ans);
    return 0;
}
