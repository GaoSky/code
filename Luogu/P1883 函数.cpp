//
//  P1883 函数.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/22.
//

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 10005
using namespace std;
double a[maxn],b[maxn],c[maxn];
int t,n;
inline double cal(double x,int k)
{
    return x*x*a[k]+x*b[k]+c[k];
}
inline double check(double x)
{
    double ans=cal(x,1);
    for(re int i=2;i<=n;++i)
        ans=max(ans,cal(x,i));
    return ans;
}
int main()
{
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d",&n);
        for(re int i=1;i<=n;++i)
        scanf("%lf%lf%lf",&a[i],&b[i],&c[i]);
        double l=0,r=1000,emp=1e-11,mid1,mid2;
        while(r-l>emp)
        {
            mid1=l+(r-l)/3.00;
            mid2=r-(r-l)/3.00;
            if(check(mid1)>check(mid2))
                l=mid1;
            else
                r=mid2;
        }
        printf("%.4lf\n",check(l));
    }
    return 0;
}
