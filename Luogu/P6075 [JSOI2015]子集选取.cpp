//
//  P6075 [JSOI2015]子集选取.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/3/22.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define mod 1000000007
using namespace std;
long long quickpow(long long a, long long b, long long c)
{
    long long ans=1;
    while(b)
    {
        if(b&1)
            ans=(ans*a)%c;
        a=(a*a)%c;
        b>>=1;
    }
    return ans;
}
int main()
{
    long long n,k;
    scanf("%lld%lld",&n,&k);
    printf("%lld",quickpow(2,n*k%(mod-1),mod));
    return 0;
}
