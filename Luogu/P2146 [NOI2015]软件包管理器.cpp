//
//  P2146 [NOI2015]软件包管理器.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/6/20.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#define re register
#define maxn 200005
#define inf 0x7fffffff
using namespace std;
int n,q,tim;
int dep[maxn],size[maxn];
int fa[maxn],dfn[maxn],top[maxn];
vector<int> son[maxn];
struct SegmentTree
{
    int l,r,sum,mark;
}tr[maxn*4];
inline void addedge(int x,int y)
{
    son[x].push_back(y);
}
void Release(int x)
{
    if(tr[x].mark!=-1)
    {
        int lch=x<<1,rch=x<<1|1;
        tr[lch].sum=(tr[lch].r-tr[lch].l+1)*tr[x].mark;
        tr[rch].sum=(tr[rch].r-tr[rch].l+1)*tr[x].mark;
        tr[lch].mark=tr[rch].mark=tr[x].mark;
        tr[x].mark=-1;
    }
}
void dfs1(int x)
{
    size[x]=1;
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(y==fa[x])
            continue;
        dep[y]=dep[x]+1,fa[y]=x;
        dfs1(y);
        size[x]+=size[y];
    }
}
void dfs2(int x,int k)
{
    int Heavy=0;
    dfn[x]=++tim,top[x]=k;
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(dep[y]>dep[x]&&size[y]>size[Heavy])
            Heavy=y;
    }
    if(Heavy==0)
        return;
    dfs2(Heavy,k);
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(dep[y]>dep[x]&&y!=Heavy)
            dfs2(y,y);
    }
}
void buildtree(int x,int l,int r)
{
    tr[x].l=l,tr[x].r=r;
    if(l==r)
        return;
    int mid=(l+r)>>1;
    buildtree(x<<1,l,mid);
    buildtree(x<<1|1,mid+1,r);
}
void modify(int x,int l,int r,int val)
{
    Release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
    {
        tr[x].sum=val*(tr[x].r-tr[x].l+1);
        tr[x].mark=val;
        return;
    }
    int mid=(tr[x].l+tr[x].r)>>1;
    if(l<=mid)
        modify(x<<1,l,r,val);
    if(mid<r)
        modify(x<<1|1,l,r,val);
    tr[x].sum=tr[x<<1].sum+tr[x<<1|1].sum;
}
void change(int x,int y,int val)
{
    while(top[x]!=top[y])
    {
        if(dep[top[x]]<dep[top[y]])
            swap(x,y);
        modify(1,dfn[top[x]],dfn[x],val);
        x=fa[top[x]];
    }
    if(dep[x]>dep[y])
        swap(x,y);
    modify(1,dfn[x],dfn[y],val);
    return;
}
int main()
{
    scanf("%d",&n);
    int tmp,tmp1;
    for(re int i=2;i<=n;++i)
        scanf("%d",&tmp),tmp++,addedge(i,tmp),addedge(tmp,i);
    dfs1(1);
    dfs2(1,1);
    buildtree(1,1,tim);
    scanf("%d",&q);
    char op[10];
    while(q--)
    {
        scanf("%s",op),scanf("%d",&tmp);
        tmp++;
        tmp1=tr[1].sum;
        if(op[0]=='i')
        {
            change(1,tmp,1);
            int tmp2=tr[1].sum;
            printf("%d\n",abs(tmp1-tmp2));
        }
        else
        {
            modify(1,dfn[tmp],dfn[tmp]
                   +size[tmp]-1,0);
            int tmp2=tr[1].sum;
            printf("%d\n",abs(tmp1-tmp2));
        }
    }
    return 0;
}
