//
//  P5686 [CSP-SJX2019]和积和.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <cctype>
#include <algorithm>
#define re register
#define maxn 500005
#define mod 1000000007
using namespace std;
int n;long long a[maxn],b[maxn];
long long suma[maxn],sumb[maxn];
long long ans;
inline long long read()
{
    long long res=0,op=1;char c=getchar();
    while(!isdigit(c)){if(c=='-') op=-1;c=getchar();}
    while(isdigit(c)){res=res*10+c-'0';c=getchar();}
    return res*op;
}
int main()
{
    long long prea=0,preb=0;
    n=read();
    for(re int i=1;i<=n;++i)
        a[i]=read(),suma[i]=(suma[i-1]+a[i])%mod;
    for(re int i=1;i<=n;++i)
        b[i]=read(),sumb[i]=(sumb[i-1]+b[i])%mod;
    for(re int i=1;i<=n;++i)
    {
        ans=(ans+(((n+1)*suma[i])%mod*sumb[i])%mod)%mod;
        prea=(prea+suma[i])%mod;
        preb=(preb+sumb[i])%mod;
    }
    long long tmp=(prea*preb)%mod;
    ans=(ans-tmp+mod)%mod;
    printf("%lld",ans);
    return 0;
}
