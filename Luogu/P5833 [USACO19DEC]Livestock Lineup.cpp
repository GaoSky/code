//
//  P5833 [USACO19DEC]Livestock Lineup.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#include <map>
using namespace std;
string s[8]={"Beatrice","Belinda","Bella","Bessie","Betsy","Blue",
    "Buttercup","Sue"};
map<string,int> mp;
char a[30],b[30];
string a1,b1;
int rule[20][2],pos[10];
int main()
{
    for(int i=0;i<8;++i)
        mp[s[i]]=i+1;
    int n;
    scanf("%d",&n);
    for(int i=1;i<=n;++i)
    {
        scanf("%s must be milked beside %s",a,b);
        a1=a,b1=b;
        rule[i][0]=mp[a1],rule[i][1]=mp[b1];
    }
    int order[8]={1,2,3,4,5,6,7,8};
    do
    {
        bool flag=false;
        for(int i=0;i<8;++i)
            pos[order[i]]=i;
        for(int i=1;i<=n;++i)
            if(abs(pos[rule[i][0]]-pos[rule[i][1]])!=1)
            {
                flag=true;
                break;
            }
        if(!flag)
            break;
    }while(next_permutation(order,order+8));
    for(int i=0;i<8;++i)
        cout<<s[order[i]-1]<<endl;
    return 0;
}
