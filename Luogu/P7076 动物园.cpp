//
//  P7076 动物园.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/17.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 1000005
#define ull unsigned long long
using namespace std;
int n,m,c,k;
ull a[maxn],l;
bool vis[100];
int main()
{
    int x,y,cnt=0;
    scanf("%d%d%d%d",&n,&m,&c,&k);
    for(re int i=1;i<=n;++i)
        scanf("%llu",&a[i]),l|=a[i];
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d",&x,&y);
        if((l&((ull)(1)<<x))==0)
            vis[x]=true;
    }
    for(re int i=0;i<k;++i)
        if(vis[i])
            cnt++;
    if(k-cnt==64)
    {
        if(n)
            printf("%llu",(ull)(-n));
        else
            printf("18446744073709551616");
    }
    else
        printf("%llu",((ull)(1)<<(k-cnt))-n);
    return 0;
}
