//
//  P3366 【模板】最小生成树.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/5.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxm 200005
#define maxn 5005
using namespace std;
struct edge
{
    int x,y,val;
}a[maxm];
int n,m,ans;
int fa[maxn];
inline int find(int x)
{
    if(fa[x]!=x)
        fa[x]=find(fa[x]);
    return fa[x];
}
bool cmp(edge a,edge b)
{
    return a.val<b.val;
}
int main()
{
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
        fa[i]=i;
    for(re int i=1;i<=m;++i)
        scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].val);
    sort(a+1,a+m+1,cmp);
    int cnt=0;
    for(re int i=1;i<=m;++i)
    {
        int f1=find(a[i].x);
        int f2=find(a[i].y);
        if(f1!=f2)
        {
            fa[f1]=f2;
            ans+=a[i].val;
            cnt++;
            if(cnt==n-1)
                break;
        }
    }
    if(cnt<n-1)
        printf("orz");
    else
        printf("%d",ans);
    return 0;
}
