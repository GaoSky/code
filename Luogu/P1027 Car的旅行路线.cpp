//
//  P1027 Car的旅行路线.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/23.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#define re register
using namespace std;
int s,A,B;double t;
double cost[405][405];
struct Node
{
    int num;double x,y,T;
    Node(){}
    Node(double _x,double _y,int _num,double _T)
    {
        x=_x,y=_y,num=_num,T=_T;
    }
};
double Distance(Node a,Node b)
{
    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}
Node Calculate(Node a,Node b,Node c)
{
    Node d;
    d.x=b.x+c.x-a.x;
    d.y=b.y+c.y-a.y;
    d.num=a.num;d.T=a.T;
    return d;
}
bool Check(Node a,Node b,Node c)
{
    if(!((b.x-a.x)*(c.x-a.x)+(b.y-a.y)*(c.y-a.y)))
        return true;
    return false;
}
Node get_the_4th_point(Node a,Node b,Node c)
{
    if(Check(a,b,c))
        return Calculate(a,b,c);
    else if(Check(b,a,c))
        return Calculate(b,a,c);
    else
        return Calculate(c,a,b);
}
vector<Node> p;
void Graph_Construction()
{
    for(re int i=0;i<p.size();++i)
        for(re int j=0;j<p.size();++j)
        {
            if(i==j)
                continue;
            Node p1=p[i],p2=p[j];
            double dis=Distance(p1,p2),tmp;
            if(p1.num==p2.num)
                tmp=dis*p1.T;
            else
                tmp=dis*t;
            cost[i][j]=cost[j][i]=tmp;
        }
}
int main()
{
    int n;
    double _x1,_y1,_x2,_y2,_x3,_y3;double tmp;
    scanf("%d",&n);
    while(n--)
    {
        memset(cost,0x3f,sizeof(cost));
        p.clear();
        scanf("%d%lf%d%d",&s,&t,&A,&B);
        for(re int i=1;i<=s;++i)
        {
            scanf("%lf%lf",&_x1,&_y1);
            scanf("%lf%lf",&_x2,&_y2);
            scanf("%lf%lf%lf",&_x3,&_y3,&tmp);
            Node p1(_x1,_y1,i,tmp),p2(_x2,_y2,i,tmp),p3(_x3,_y3,i,tmp);
            Node p4=get_the_4th_point(p1,p2,p3);
            p.push_back(p1);p.push_back(p2);
            p.push_back(p3);p.push_back(p4);
        }
        Graph_Construction();
        for(re int k=0;k<p.size();++k)
            for(re int i=0;i<p.size();++i)
                for(re int j=0;j<p.size();++j)
                    cost[i][j]=min(cost[i][j],cost[i][k]+cost[k][j]);
        double minn=0x7fffffff;
        for(re int i=0;i<p.size();++i)
            for(re int j=0;j<p.size();++j)
                if(p[i].num==A&&p[j].num==B)
                    minn=min(minn,cost[i][j]);
        printf("%.1lf\n",minn);
    }
    return 0;
}
