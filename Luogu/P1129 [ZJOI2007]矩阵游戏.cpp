//
//  P1129 [ZJOI2007]矩阵游戏.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/9/10.(Reconstructed) 2019/3/29(First)
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#define maxn 405
#define re register
using namespace std;
vector<int> son[maxn*maxn];
int n,t,timemark,link[maxn*maxn],cnt;
int vis[maxn*maxn];
bool dfs(int x)
{
    for(re int i=0;i<son[x].size();++i)
    {
        int to=son[x][i];
        if(vis[to]!=timemark)
        {
            vis[to]=timemark;
            if(!link[to]||dfs(link[to]))
            {
                link[to]=x;
                return true;
            }
        }
    }
    return false;
}
int main()
{
    scanf("%d",&t);
    while(t--)
    {
        int a;
        scanf("%d",&n);
        for(re int i=1;i<=n;++i)
            for(re int j=1;j<=n;++j)
            {
                scanf("%d",&a);
                if(a==1)
                    son[i].push_back(j+n);
            }
        for(re int i=1;i<=n;++i)
        {
            ++timemark;
            if(dfs(i))
                ++cnt;
        }
        if(cnt>=n)
            printf("Yes\n");
        else
            printf("No\n");
        memset(son,0,sizeof(son));
        memset(link,0,sizeof(link));
        timemark=0,cnt=0;
        memset(vis,0,sizeof(vis));
    }
    return 0;
}

