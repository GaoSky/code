//
//  P1349 广义斐波那契数列_1.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/3/22.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define ll long long
using namespace std;
struct Matrix
{
    ll a[3][3];
};
Matrix mul_matric(Matrix a,Matrix b,ll mod)
{
    Matrix c;
    for(int i=1;i<=2;i++)
        for(int j=1;j<=2;j++)
        {
            c.a[i][j]=0;
            for(int p=1;p<=2;p++)
            {
                c.a[i][j]+=a.a[i][p]*b.a[p][j];
                c.a[i][j]%=mod;
            }
        }
    return c;
}
Matrix re_unit()
{
    Matrix ans;
    for(int i=1;i<=2;i++)
        for(int j=1;j<=2;j++)
        {
            if(i==j)
                ans.a[i][j]=1;
            else
                ans.a[i][j]=0;
        }
    return ans;
}
Matrix Pow_matric(Matrix a,ll y,ll mod)
{
    Matrix res=re_unit(),temp=a;
    for(;y;y/=2)
    {
        if(y&1)
            res=mul_matric(res, temp, mod);
        temp=mul_matric(temp, temp, mod);
    }
    return res;
}
int p,q,a1,a2,k,m;
int main()
{
    scanf("%d%d%d%d%d%d",&p,&q,&a1,&a2,&k,&m);
    if(k==1||k==2)
    {
        putchar('1');
        return 0;
    }
    Matrix base,ans,tmp;
    base.a[1][1]=p;
    base.a[1][2]=q;
    base.a[2][1]=1;
    base.a[2][2]=0;
    ans=Pow_matric(base,k-2,m);
    tmp.a[1][1]=a2;
    tmp.a[2][1]=a1;
    ans=mul_matric(ans,tmp,m);
    printf("%lld",ans.a[1][1]%m);
    return 0;
}
