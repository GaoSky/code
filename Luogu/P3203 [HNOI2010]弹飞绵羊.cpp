//
//  C23-分块-弹飞绵羊.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/4.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <cmath>
#define re register
#define maxn 200005
using namespace std;
int n,m;
int a[maxn],x[maxn],y[maxn];
int l[maxn],r[maxn],pos[maxn];
int query(int k)
{
    int ans=0;
    while(k<=n)
        ans+=x[k],k=y[k];
    return ans;
}
void modify(int p,int q)
{
    a[p]=q;
    for(re int i=p;i>=r[pos[p]-1];--i)
    {
        if(i+a[i]>r[pos[i]])
            x[i]=1,y[i]=i+a[i];
        else
        {
            x[i]=x[i+a[i]]+1;
            y[i]=y[i+a[i]];
        }
    }
}
int main()
{
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
        scanf("%d",&a[i]);
    int k=sqrt(n),t=n/k;
    if(n%k)
        t++;
    for(re int i=1;i<=t;++i)
        l[i]=(i-1)*k+1,r[i]=i*k;
    r[t]=n;
    for(re int i=1;i<=n;++i)
        pos[i]=(i-1)/k+1;
    for(re int i=n;i>=1;--i)
    {
        if(i+a[i]>r[pos[i]])
            x[i]=1,y[i]=i+a[i];
        else
        {
            x[i]=x[i+a[i]]+1;
            y[i]=y[i+a[i]];
        }
    }
    scanf("%d",&m);
    while(m--)
    {
        int op,p,q;
        scanf("%d",&op);
        if(op==1)
        {
            scanf("%d",&p);
            printf("%d\n",query(p+1));
        }
        else
        {
            scanf("%d%d",&p,&q);
            modify(p+1,q);
        }
    }
    return 0;
}
