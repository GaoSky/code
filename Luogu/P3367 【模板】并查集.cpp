//
//  P3367 【模板】并查集.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/3.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 10005
using namespace std;
int fa[maxn];
int n,m;
inline int find(int x)
{
    if(x!=fa[x])
        fa[x]=find(fa[x]);
    return fa[x];
}
inline void merge(int x,int y)
{
    int f1=find(x),f2=find(y);
    if(f1!=f2)
        fa[f1]=f2;
}
inline bool judge(int x,int y)
{
    int f1=find(x),f2=find(y);
    return f1==f2;
}
int main()
{
    int x,y,z;
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
        fa[i]=i;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&z,&x,&y);
        if(z==1)
            merge(x,y);
        else
            printf(judge(x,y)?"Y\n":"N\n");
    }
    return 0;
}
