//
//  P1226 【模板】快速幂||取余运算.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/3.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define ll long long
using namespace std;
ll quickpow(ll a,ll b,ll p)
{
    if(b==0)
        return 1%p;
    ll res=1;
    while(b)
    {
        if(b&1)
            res=res*a%p;
        a=a*a%p;
        b>>=1;
    }
    return res;
}
int main()
{
    ll a,b,c;
    scanf("%lld%lld%lld",&a,&b,&c);
    printf("%d^%d mod %d=%lld",a,b,c,quickpow(a,b,c));
    return 0;
}
