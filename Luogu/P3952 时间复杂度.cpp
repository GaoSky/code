//
//  P3952 时间复杂度.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/24.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <stack>
#define re register
using namespace std;
string o,code[105];
int t,l,w;
int getnum(int &pos,string c)
{
    int res=0,len=c.size();
    while((c[pos]<'0'||c[pos]>'9')&&pos<len)
    {
        pos++;
        if(c[pos-1]=='n')
            return 1000000;
    }
    while(c[pos]>='0'&&c[pos]<='9')
    {
        res*=10,res+=c[pos]-'0';
        pos++;
    }
    return res;
}
int geto()
{
    int res=0,pos=3,len=o.size();
    if(o[2]=='n')
        res=getnum(pos,o);
    else
        res=0;
    return res;
}
int judge()
{
    int res=0,now=0,pos;
    int a,b;
    stack<int> s;
    int flag=-1;
    bool vis[26]={0},ctb[26]={0};
    for(re int i=1;i<=l;++i)
    {
        if(code[i][0]=='F')
        {
            int k=code[i][2]-'a';
            if(vis[k])
                return -1;
            vis[k]=1;s.push(k);pos=4;
            a=getnum(pos,code[i]),b=getnum(pos,code[i]);
            if(b-a>1000)
                if(flag==-1)
                {
                    now++;
                    res=max(res,now);
                    ctb[k]=true;
                }
            if(a>b)
                if(flag==-1)
                    flag=k;
        }
        if(code[i][0]=='E')
        {
            if(s.empty())
                return -1;
            int k=s.top();
            s.pop();vis[k]=false;
            if(flag==k)
                flag=-1;
            if(ctb[k])
                ctb[k]=false,now--;
        }
    }
    if(s.size())
        return -1;
    return res;
}
int main()
{
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d ",&l),getline(cin,o);
        for(re int i=1;i<=l;++i)
            getline(cin,code[i]);
        w=geto();
        int p=judge();
        if(p==-1)
            printf("ERR\n");
        else if(p==w)
            printf("Yes\n");
        else
            printf("No\n");
    }
    return 0;
}
