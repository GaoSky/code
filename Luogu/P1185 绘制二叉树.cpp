//
//  P1185 绘制二叉树.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/22.
//

#include <stdio.h>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cmath>
#define re register
#define maxn 9005
using namespace std;
char mp[maxn][maxn];
int m,n;
struct pos
{
    int x,y;
}book[11][520];
void dfs(int x,int y)
{
    if(x==1)
    {
        mp[x][y]=mp[x+1][y-1]=mp[x+1][y+1]=0;
        return;
    }
    int tx=x,ty=y;
    if(mp[tx+1][ty-1]!=0)
    {
        tx++,ty--;
        while(mp[tx][ty]=='\\')
            mp[tx][ty]=0,tx++,ty--;
    }
    else
    {
        tx++,ty++;
        while(mp[tx][ty]=='/')
            mp[tx][ty]=0,tx++,ty++;
    }
    tx=x,ty=y;
    while(mp[tx][ty]!=0)
    {
        mp[tx][ty]=0;
        tx--,ty--;
        if(mp[tx][ty]=='o')
            dfs(tx,ty);
    }
    tx=x,ty=y,mp[tx][ty]='o';
    while(mp[tx][ty]!=0)
    {
        mp[tx][ty]=0;
        tx--,ty++;
        if(mp[tx][ty]=='o')
            dfs(tx,ty);
    }
}
int main()
{
    scanf("%d%d",&m,&n);
    int num=pow(2,m-1),k=1;
    mp[1][1]='o';book[m][1]=(pos){1,1};
    for(re int i=2;i<=num;++i)
    {
        if(i&1)
        {
            k++;
            mp[1][++k]='o';
            book[m][i]=(pos){1,k};
        }
        else
        {
            k+=3;
            mp[1][++k]='o';
            book[m][i]=(pos){1,k};
        }
    }
    int level=1,top=1,now=1,NC;
    int x,y;bool flag;
    for(re int cnt=1;cnt<m;++cnt)
    {
        NC=0;
        for(re int i=top;i<=k;)
        {
            flag=false;
            if(i==top)
                flag=true;
            level=now;
            x=i,y=i+2;
            while(mp[now][y]!='o'&&y<=k) y++;
            if(y>k) break;
            i=y+1;
            x+=1,y-=1;
            for(;x!=y;x+=1,y-=1)
            {
                level++;
                mp[level][x]='/',mp[level][y]='\\';
            }
            level++;
            mp[level][x]='o';NC++;book[m-cnt][NC]=(pos){level,x};
            if(flag)
                top=x;
            while(mp[now][i]!='o'&&i<=k) i++;
        }
        now=level;
    }
    int qx,qy;
    for(re int i=1;i<=n;++i)
    {
        scanf("%d%d",&qx,&qy);
        dfs(book[qx][qy].x,book[qx][qy].y);
    }
    for(re int i=level;i>=1;--i)
    {
        for(re int j=1;j<=k;++j)
        {
            if(mp[i][j]==0)
                putchar(' ');
            else
                putchar(mp[i][j]);
        }
        putchar('\n');
    }
    return 0;
}
