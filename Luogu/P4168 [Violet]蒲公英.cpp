//
//  P4168 [Violet]蒲公英.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/12.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <cmath>
#define maxn 40005
#define re register
using namespace std;
int c[40][40][maxn],f[40][40],d[40][40],a[maxn],b[maxn],fa[maxn],fb[maxn];
int n,m,tot,x,y,ans,L,R,cnt,num;
int t,len,st[40],ed[40];
inline void init()
{
    t=(int)pow(n*1.00,1.00/3.00);
    if(t)
        len=n/t;
    for(re int i=1;i<=t;++i)
        st[i]=(i-1)*len+1,ed[i]=i*len;
    if(ed[t]<n)
        st[t+1]=ed[t]+1,ed[++t]=n;
    memcpy(fa,a,sizeof(a));
    sort(fa+1,fa+n+1);
    for(re int i=1;i<=n;++i)
        if(i==1||fa[i]!=fa[i-1])
            fb[++tot]=fa[i];
    for(re int i=1;i<=n;++i)
        b[i]=lower_bound(fb+1,fb+tot+1,a[i])-fb;
    for(re int i=1;i<=t;++i)
        for(re int j=i;j<=t;++j)
        {
            for(re int k=st[i];k<=ed[j];++k)
                c[i][j][b[k]]++;
            for(re int k=1;k<=tot;++k)
                if(c[i][j][k]>f[i][j]||c[i][j][k]==f[i][j]&&k<d[i][j])
                    f[i][j]=c[i][j][k],d[i][j]=k;
        }
}

inline void update(int i)
{
    c[L][R][b[i]]++;
    if(c[L][R][b[i]]>cnt||c[L][R][b[i]]==cnt&&b[i]<num)
        cnt=c[L][R][b[i]],num=b[i];
}

inline int solve(int x,int y)
{
    int r,l;
    if(x>y)
        swap(x,y);
    for(re int i=1;i<=t;++i)
        if(x<=ed[i])
        {
            l=i;
            break;
        }
    for(re int i=t;i;--i)
        if(y>=st[i])
        {
            r=i;
            break;
        }
    if(l+1<=r-1)
        L=l+1,R=r-1;
    else
        L=R=0;
    cnt=f[L][R],num=d[L][R];
    if(l==r)
    {
        for(re int i=x;i<=y;++i)
            update(i);
        for(re int i=x;i<=y;++i)
            c[L][R][b[i]]--;
    }
    else
    {
        for(re int i=x;i<=ed[l];++i)
            update(i);
        for(re int i=st[r];i<=y;++i)
            update(i);
        for(re int i=x;i<=ed[l];++i)
            c[L][R][b[i]]--;
        for(re int i=st[r];i<=y;++i)
            c[L][R][b[i]]--;
    }
    return fb[num];
}

int main()
{
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
        scanf("%d",&a[i]);
    init();
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d",&x,&y);
        ans=solve((x+ans-1)%n+1,(y+ans-1)%n+1);
        printf("%d\n",ans);
    }
    return 0;
}
