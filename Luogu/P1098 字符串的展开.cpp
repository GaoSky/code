//
//  P1098 字符串的展开.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/26.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <string>
#include <iostream>
#include <algorithm>
#define re register
using namespace std;
int main()
{
    int p1,p2,p3;
    std::ios::sync_with_stdio(false);
    cin.tie(0),cout.tie(0);
    string ori;
    cin>>p1>>p2>>p3;
    cin.get();
    cin>>ori;
    int len=ori.size();
    for(re int i=0;i<len;++i)
    {
        string tmp="";
        if(ori[i]!='-')
        {
            cout<<ori[i];
            continue;
        }
        if(ori[i+1]=='-'||ori[i-1]=='-')
        {
            cout<<'-';
            continue;
        }
        if(i==0||i==len-1)
        {
            cout<<'-';
            continue;
        }
        if(ori[i-1]+1==ori[i+1])
            continue;
        if(ori[i-1]>=ori[i+1]||(!isdigit(ori[i-1])&&isdigit(ori[i+1]))||(isdigit(ori[i-1])&&!isdigit(ori[i+1])))
        {
            cout<<'-';
            continue;
        }
        for(re char j=ori[i-1]+1;j<ori[i+1];++j)
            for(re int k=1;k<=p2;++k)
                tmp+=j;
        if(p1==2&&!isdigit(ori[i-1]))
            for(re int j=0;j<tmp.size();++j)
                tmp[j]-=32;
        else if(p1==3)
        {
            for(re int j=0;j<tmp.size();++j)
                cout<<'*';
            continue;
        }
        if(p3==2)
            reverse(tmp.begin(),tmp.end());
        cout<<tmp;
    }
    return 0;
}
