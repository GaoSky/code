//
//  P5831 [USACO19DEC]Cow Gymnastics.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
using namespace std;
int a[25];
int book[25][25];
int ans=0;
int main()
{
    int n,k;
    scanf("%d%d",&k,&n);
    for(int t=1;t<=k;++t)
    {
        for(int i=1;i<=n;++i)
            scanf("%d",&a[i]);
        for(int i=1;i<=n;++i)
            for(int j=i+1;j<=n;++j)
                book[a[i]][a[j]]++;
    }
    for(int i=1;i<=n;++i)
        for(int j=1;j<=n;++j)
            if(book[i][j]==k)
                ans++;
    printf("%d",ans);
    return 0;
}
