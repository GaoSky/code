//
//  P5658 括号树.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <cctype>
#define re register
#define maxn 500005
using namespace std;
long long f[maxn],last[maxn],line[maxn];
char k[maxn];
int fa[maxn],n;
long long ans;
inline int read()
{
    int res=0,op=1;char c=getchar();
    while(!isdigit(c)){if(c=='-') op=-1;c=getchar();}
    while(isdigit(c)){res=res*10+c-'0';c=getchar();}
    return res*op;
}
int main()
{
    n=read();
    scanf("%s",k+1);
    for(re int i=2;i<=n;++i)
        fa[i]=read();
    for(re int i=1;i<=n;++i)
    {
        int prev=fa[i];
        f[i]=f[prev];
        last[i]=last[prev];
        if(k[i]=='(')
            last[i]=i;
        else if(k[i]==')'&&last[i])
        {
            line[i]=line[fa[last[i]]]+1;
            last[i]=last[fa[last[i]]];
            f[i]+=line[i];
        }
        ans^=(i*f[i]);
    }
    printf("%lld",ans);
    return 0;
}
