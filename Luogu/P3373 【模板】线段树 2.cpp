//
//  P3373 【模板】线段树 2.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/2.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <iostream>
#include <cstring>
#include <cstdio>
#define maxn 100005
using namespace std;
struct point
{
    long long l,r;
    long long val,add,mul;
}tr[maxn*4];
int m,n,a,b,c;long long p[maxn];
int mod;
char op[10];
void buildtree(int x,int l,int r)
{
    tr[x].mul=1;
    tr[x].l=l;
    tr[x].r=r;
    if(l==r)
    {
        tr[x].val=p[l]%mod;
        return;
    }
    int lch=x<<1,rch=x<<1|1;
    int mid=(l+r)>>1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
    tr[x].val=(tr[lch].val+tr[rch].val)%mod;
}
void release(int x)
{
    if((tr[x].add||tr[x].mul!=1)&&tr[x].l<tr[x].r)
    {
        long long mid=(tr[x].l+tr[x].r)>>1;
        int lch=x<<1,rch=x<<1|1;
        tr[lch].val=((tr[lch].val*tr[x].mul)%mod+tr[x].add*(mid-tr[x].l+1))%mod;
        tr[rch].val=((tr[rch].val*tr[x].mul)%mod+tr[x].add*(tr[rch].r-mid))%mod;
        tr[lch].mul=(tr[lch].mul*tr[x].mul)%mod;
        tr[rch].mul=(tr[rch].mul*tr[x].mul)%mod;
        tr[lch].add=((tr[lch].add*tr[x].mul)%mod+tr[x].add)%mod;
        tr[rch].add=((tr[rch].add*tr[x].mul)%mod+tr[x].add)%mod;
    }
    tr[x].add=0;
    tr[x].mul=1;
    return;
}
void modify1(int x,int l,int r,long long k)
{
    if(r<tr[x].l||l>tr[x].r)
        return;
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
    {
        tr[x].val=(tr[x].val+k*(tr[x].r-tr[x].l+1))%mod;
        tr[x].add=(tr[x].add+k)%mod;
        return;
    }
    modify1(x<<1,l,r,k);
    modify1(x<<1|1,l,r,k);
    tr[x].val=(tr[x<<1].val+tr[x<<1|1].val)%mod;
}
void modify2(int x,int l,int r,long long k)
{
    if(r<tr[x].l||l>tr[x].r)
        return;
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
    {
        tr[x].val=(tr[x].val*k)%mod;
        tr[x].mul=(tr[x].mul*k)%mod;
        tr[x].add=(tr[x].add*k)%mod;
        return;
    }
    modify2(x<<1,l,r,k);
    modify2(x<<1|1,l,r,k);
    tr[x].val=(tr[x<<1].val+tr[x<<1|1].val)%mod;
}
long long query(int x,int l,int r)
{
    if(r<tr[x].l||l>tr[x].r)
        return 0;
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
        return tr[x].val%mod;
    long long mid=(tr[x].l+tr[x].r)>>1;
    long long ans=0;
    if(l<=mid)
        ans=(ans+query(x<<1,l,r))%mod;
    if(r>mid)
        ans=(ans+query(x<<1|1,l,r))%mod;
    return ans%mod;
}
int main()
{
    scanf("%d%d%d",&n,&m,&mod);
    for(int i=1;i<=n;i++)
        scanf("%lld",&p[i]);
    buildtree(1,1,n);
    long long k;
    for(int i=1;i<=m;i++)
    {
        scanf("%d",&a);
        if(a==1)
        {
            scanf("%d%d%lld",&b,&c,&k);
            modify2(1,b,c,k);
        }
        else if(a==2)
        {
            scanf("%d%d%lld",&b,&c,&k);
            modify1(1,b,c,k);
        }
        else
        {
            scanf("%d%d",&b,&c);
            printf("%lld\n",query(1,b,c));
        }
    }
    return 0;
}
