//
//  P3372 【模板】线段树 1.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/3.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
struct SegmentTree
{
    long long l,r,val,mark;
}tr[maxn<<2];
int n,m,p[maxn];
inline void buildtree(int x,int l,int r)
{
    tr[x].l=l,tr[x].r=r;
    if(l==r)
    {
        tr[x].val=p[l];
        return;
    }
    int mid=(l+r)>>1,lch=x<<1,rch=x<<1|1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
    tr[x].val=tr[lch].val+tr[rch].val;
}
inline void release(int x)
{
    if(tr[x].mark&&tr[x].l<tr[x].r)
    {
        int lch=x<<1,rch=x<<1|1;
        tr[lch].mark+=tr[x].mark;
        tr[rch].mark+=tr[x].mark;
        tr[lch].val+=tr[x].mark*(long long)(tr[lch].r-tr[lch].l+1);
        tr[rch].val+=tr[x].mark*(long long)(tr[rch].r-tr[rch].l+1);
    }
    tr[x].mark=0;
}
inline void modify(int x,int l,int r,int k)
{
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
    {
        tr[x].val+=k*(long long)(tr[x].r-tr[x].l+1);
        tr[x].mark+=k;
        return;
    }
    int mid=(tr[x].l+tr[x].r)>>1,lch=x<<1,rch=x<<1|1;
    if(l<=mid)
        modify(lch,l,r,k);
    if(r>mid)
        modify(rch,l,r,k);
    tr[x].val=tr[lch].val+tr[rch].val;
}
inline long long query(int x,int l,int r)
{
    release(x);
    if(l<=tr[x].l&&tr[x].r<=r)
        return tr[x].val;
    int mid=(tr[x].l+tr[x].r)>>1,lch=x<<1,rch=x<<1|1;
    long long ans=0;
    if(l<=mid)
        ans+=query(lch,l,r);
    if(r>mid)
        ans+=query(rch,l,r);
    return ans;
}
int main()
{
    int op,x,y,z;
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
        scanf("%d",&p[i]);
    buildtree(1,1,n);
    while(m--)
    {
        scanf("%d",&op);
        if(op==1)
        {
            scanf("%d%d%d",&x,&y,&z);
            modify(1,x,y,z);
        }
        else
        {
            scanf("%d%d",&x,&y);
            printf("%lld\n",query(1,x,y));
        }
    }
    return 0;
}
