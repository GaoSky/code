//
//  P1131 [ZJOI2007]时态同步.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/28.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#define re register
#define maxn 500005
using namespace std;
vector<int> son[maxn],v[maxn];
int n,root;long long ans;
int maxx[maxn];
void dfs(int x,int fa)
{
    for(re int i=0;i<son[x].size();++i)
        if(son[x][i]!=fa)
            dfs(son[x][i],x);
    for(re int i=0;i<son[x].size();++i)
        if(son[x][i]!=fa)
            maxx[x]=max(maxx[x],v[x][i]);
    for(re int i=0;i<son[x].size();++i)
        if(son[x][i]!=fa)
            ans+=(maxx[x]-v[x][i]);
    for(re int i=0;i<son[fa].size();++i)
        if(son[fa][i]==x)
        {
            v[fa][i]+=maxx[x];
            break;
        }
}
int main()
{
    int x,y,z;
    scanf("%d%d",&n,&root);
    for(re int i=1;i<n;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        son[x].push_back(y);
        v[x].push_back(z);
        son[y].push_back(x);
        v[y].push_back(z);
    }
    dfs(root,0);
    printf("%lld",ans);
    return 0;
}
