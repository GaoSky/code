//
//  P1919 【模板】A*B Problem升级版（FFT快速傅里叶）.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/26.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>
#define re register
#define maxn 3000005
using namespace std;
const double PI=acos(-1.0);
struct Complex
{
    double r,i;
    Complex() {r=i=0;}
    Complex(double x,double y){r=x,i=y;}
    Complex operator + (Complex &x) {return Complex(r+x.r,i+x.i);}
    Complex operator - (Complex &x) {return Complex(r-x.r,i-x.i);}
    Complex operator * (Complex &x) {return Complex(r*x.r-i*x.i,r*x.i+i*x.r);}
    void operator += (Complex &x) {r+=x.r;i+=x.i;};
    void operator *= (Complex &x) {double t=r;r=r*x.r-i*x.i;i=t*x.i+i*x.r;}
}f[maxn],g[maxn];
char a[maxn],b[maxn];
int n,m,lim=1,l,r[maxn],ans[maxn];
void FastFourierTransform(Complex *a,int op)
{
    for(re int i=0;i<lim;++i)
        if(i<r[i])
            swap(a[i],a[r[i]]);
    for(re int i=1;i<lim;i<<=1)
    {
        Complex W=Complex(cos(PI/i),(double)op*sin(PI/i));
        for(re int j=0;j<lim;j+=(i<<1))
        {
            Complex w(1,0);
            for(re int k=0;k<i;++k,w*=W)
            {
                Complex tx=a[j+k],ty=w*a[i+j+k];
                a[j+k]=tx+ty;
                a[i+j+k]=tx-ty;
            }
        }
    }
}
int main()
{
    scanf("%s%s",a,b);
    for(re int i=strlen(a)-1;i>=0;--i)
        f[n++].r=a[i]-'0';
    for(re int i=strlen(b)-1;i>=0;--i)
        g[m++].r=b[i]-'0';
    while(lim<n+m)
        lim<<=1,l++;
    for(re int i=0;i<=lim;++i)
        r[i]=(r[i>>1]>>1)|((i&1)<<(l-1));
    FastFourierTransform(f,1);
    FastFourierTransform(g,1);
    for(re int i=0;i<=lim;++i)
        f[i]=f[i]*g[i];
    FastFourierTransform(f,-1);
    for(re int i=0;i<=lim;++i)
    {
        ans[i]+=(f[i].r/(double)lim+0.5);
        if(ans[i]>=10)
        {
            ans[i+1]+=ans[i]/10;
            ans[i]%=10;
            lim+=(i==lim);
        }
    }
    while(ans[lim]==0&&lim>=1) lim--;
    for(re int i=lim;i>=0;--i)
        printf("%d",ans[i]);
    return 0;
}
