//
//  P2347 砝码称重.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/25.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#define re register
using namespace std;
const int weight[7]={0,1,2,3,5,10,20};
int a[7],ans;
bool f[1005];
int main()
{
    for(re int i=1;i<=6;++i)
        scanf("%d",&a[i]);
    f[0]=true;
    for(re int i=1;i<=6;++i)
        for(re int j=1;j<=a[i];++j)
            for(re int k=1000;k>=0;--k)
                if(f[k])
                    f[k+weight[i]]=true;
    for(re int i=1;i<=1000;++i)
        ans+=f[i];
    printf("Total=%d",ans);
    return 0;
}
