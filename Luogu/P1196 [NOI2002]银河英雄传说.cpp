//
//  P1196 [NOI2002]银河英雄传说.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/4/11.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 30005
using namespace std;
int fa[maxn],d[maxn],size[maxn];
int t;
int find(int x)
{
    if(fa[x]==x)
        return x;
    int root=find(fa[x]);
    d[x]+=d[fa[x]];
    return fa[x]=root;
}
void merge(int x,int y)
{
    x=find(x),y=find(y);
    fa[x]=y;
    d[x]=size[y];
    size[y]+=size[x];
}
int main()
{
    scanf("%d",&t);
    for(re int i=1;i<maxn;++i)
    {
        fa[i]=i;
        d[i]=0;
        size[i]=1;
    }
    char op[2];int x,y;
    for(re int i=1;i<=t;++i)
    {
        scanf("%s",op);
        scanf("%d%d",&x,&y);
        if(op[0]=='M')
            merge(x,y);
        else
        {
            if(find(x)==find(y))
                printf("%d\n",abs(d[x]-d[y])-1);
            else
                printf("-1\n");
        }
    }
    return 0;
}
