//
//  P2740 [USACO4.2]草地排水Drainage Ditches.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/28.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#define re register
#define inf 0x7fffffff
#define maxn 10010
#define maxm 100010
using namespace std;
int n,m,s,t;
int ans,d[maxn];
struct Edge
{
    int to,val,rev;
    Edge(int x,int y,int z)
    {
        to=x;val=y;rev=z;
    }
};
vector<Edge> son[maxn];
inline void addedge(int x,int y,int z)
{
    son[x].push_back(Edge(y,z,son[y].size()));
    son[y].push_back(Edge(x,0,son[x].size()-1));
}
inline bool bfs()
{
    memset(d,-1,sizeof(d));
    queue<int> q;
    q.push(s);
    d[s]=0;
    while(!q.empty())
    {
        int x=q.front();
        q.pop();
        for(re int i=0;i<son[x].size();++i)
        {
            int y=son[x][i].to;
            if(d[y]==-1&&son[x][i].val>0)
            {
                q.push(y);
                d[y]=d[x]+1;
            }
        }
    }
    if(d[t]==-1)
        return false;
    else
        return true;
}
inline int dfs(int x,int low)
{
    if(x==t||low==0)
        return low;
    int totflow=0;
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i].to;
        int rev=son[x][i].rev;
        if(d[y]==d[x]+1&&son[x][i].val>0)
        {
            int minval=dfs(y,min(low,son[x][i].val));
            son[x][i].val-=minval;
            son[y][rev].val+=minval;
            low-=minval;
            totflow+=minval;
            if(low==0)
                return totflow;
        }
    }
    if(low!=0)
        d[x]=-1;
    return totflow;
}
void dinic()
{
    while(bfs())
        ans+=dfs(s,inf);
}
int main()
{
    scanf("%d%d",&m,&n);
    s=1,t=n;
    int x,y,z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        addedge(x,y,z);
    }
    dinic();
    printf("%d",ans);
    return 0;
}