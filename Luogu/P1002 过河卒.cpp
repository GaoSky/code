//
//  P1002 过河卒.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/28.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#define re register
const int nex[2][9]={{0,-2,-1,1,2,2,1,-1,-2},{0,1,2,2,1,-1,-2,-2,-1}};
using namespace std;
long long f[25][25]={1};
bool flag[25][25];
int main()
{
    int sx,sy,px,py;
    scanf("%d%d%d%d",&sx,&sy,&px,&py);
    for(re int i=0;i<=8;++i)
    {
        int tx=px+nex[0][i];
        int ty=py+nex[1][i];
        if(tx>=0&&tx<=sx&&ty>=0&&ty<=sy)
            flag[tx][ty]=true;
    }
    for(re int i=0;i<=sx;++i)
        for(re int j=0;j<=sy;++j)
        {
            if(i>0)
                f[i][j]+=f[i-1][j];
            if(j>0)
                f[i][j]+=f[i][j-1];
            f[i][j]*=(!flag[i][j]);
        }
    printf("%lld",f[sx][sy]);
    return 0;
}
