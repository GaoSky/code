//
//  P3386 【模板】二分图最大匹配.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/9/10.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#define re register
#define maxn 1005
using namespace std;
vector<int> son[maxn];
int n,m,e,timemark,link[maxn],cnt;
int vis[maxn];
bool dfs(int x)
{
    for(re int i=0;i<son[x].size();++i)
    {
        int to=son[x][i];
        if(vis[to]!=timemark)
        {
            vis[to]=timemark;
            if(!link[to]||dfs(link[to]))
            {
                link[to]=x;
                return true;
            }
        }
    }
    return false;
}
int main()
{
    scanf("%d%d%d",&n,&m,&e);
    for(re int i=1;i<=e;++i)
    {
        int x,y;
        scanf("%d%d",&x,&y);
        son[x].push_back(y);
    }
    for(re int i=1;i<=n;++i)
    {
        ++timemark;
        if(dfs(i))
            ++cnt;
    }
    printf("%d",cnt);
    return 0;
}
