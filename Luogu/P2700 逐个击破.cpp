//
//  P2700 逐个击破.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/10/17.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 100005
using namespace std;
struct Edge
{
    int x,y,val;
}e[maxn];
int n,k,fa[maxn];long long ans;
bool vis[maxn];
inline int find(int x)
{
    return fa[x]=(fa[x]==x)?x:find(fa[x]);
}
inline bool cmp(Edge a,Edge b)
{
    return a.val>b.val;
}
int main()
{
    int x,y,z;
    scanf("%d%d",&n,&k);
    for(re int i=1;i<=n;++i)
        fa[i]=i;
    for(re int i=1;i<=k;++i)
        scanf("%d",&x),vis[x]=true;
    for(re int i=1;i<n;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        e[i].x=x,e[i].y=y,e[i].val=z,ans+=z;
    }
    sort(e+1,e+n+1,cmp);
    for(re int i=1;i<n;++i)
    {
        int f1=find(e[i].x),f2=find(e[i].y);
        if(vis[f1]&&vis[f2])
            continue;
        fa[f1]=f2,ans-=e[i].val;
        if(vis[f1]||vis[f2])
            vis[f1]=vis[f2]=true;
    }
    printf("%lld",ans);
    return 0;
}
