//
//  P3371 【模板】单源最短路径（弱化版）.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/28.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
#define re register
#define maxn 100010
using namespace std;
int n,m,s;
int dis[maxn];
vector<int> son[maxn],v[maxn];
bool vis[maxn];
void addedge(int x,int y,int z)
{
    son[x].push_back(y);
    v[x].push_back(z);
}
struct Node
{
    int x,dis;
    bool operator < (const Node &a)const
    {
        return dis>a.dis;
    }
};priority_queue<Node> q;
void Dijkstra(int S)
{
    for(re int i=1;i<=n;++i)
        dis[i]=0x7fffffff;
    dis[S]=0,q.push((Node){S,0});
    while(!q.empty())
    {
        Node prt=q.top();q.pop();
        int rt=prt.x;
        if(vis[rt])
            continue;
        vis[rt]=true;
        for(re int i=0;i<son[rt].size();++i)
        {
            int to=son[rt][i];
            if(dis[to]>dis[rt]+v[rt][i])
            {
                dis[to]=dis[rt]+v[rt][i];
                q.push((Node){to,dis[to]});
            }
        }
    }
}
int main()
{
    scanf("%d%d%d",&n,&m,&s);
    int x,y,z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        addedge(x,y,z);
    }
    Dijkstra(s);
    for(re int i=1;i<=n;++i)
        printf("%d ",dis[i]);
    return 0;
}
