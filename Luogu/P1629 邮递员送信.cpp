//
//  P1629 邮递员送信.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/8/1.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#define re register
#define maxn 1005
using namespace std;
int n,m;
int dis[maxn],vis[maxn],tim;
vector<int> son[maxn],v[maxn];
vector<int> _son[maxn],_v[maxn];
void addedge(int x,int y,int z)
{
    son[x].push_back(y);
    v[x].push_back(z);
    _son[y].push_back(x);
    _v[y].push_back(z);
}
struct Node
{
    int x,dis;
    bool operator < (const Node &a)const
    {
        return dis>a.dis;
    }
};priority_queue<Node> q;
void Dijkstra(int S)
{
    for(re int i=1;i<=n;++i)
        dis[i]=0x7fffffff;
    dis[S]=0,q.push((Node){S,0}),tim++;
    while(!q.empty())
    {
        Node prt=q.top();q.pop();
        int rt=prt.x;
        if(vis[rt]==tim)
            continue;
        vis[rt]=tim;
        for(re int i=0;i<son[rt].size();++i)
        {
            int to=son[rt][i];
            if(dis[to]>dis[rt]+v[rt][i])
            {
                dis[to]=dis[rt]+v[rt][i];
                q.push((Node){to,dis[to]});
            }
        }
    }
}
void _Dijkstra(int S)
{
    for(re int i=1;i<=n;++i)
        dis[i]=0x7fffffff;
    dis[S]=0,q.push((Node){S,0}),tim++;
    while(!q.empty())
    {
        Node prt=q.top();q.pop();
        int rt=prt.x;
        if(vis[rt]==tim)
            continue;
        vis[rt]=tim;
        for(re int i=0;i<_son[rt].size();++i)
        {
            int to=_son[rt][i];
            if(dis[to]>dis[rt]+_v[rt][i])
            {
                dis[to]=dis[rt]+_v[rt][i];
                q.push((Node){to,dis[to]});
            }
        }
    }
}
int main()
{
    scanf("%d%d",&n,&m);
    int x,y,z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        addedge(x,y,z);
    }
    int ans=0;
    Dijkstra(1);
    for(re int i=2;i<=n;++i)
        ans+=dis[i];
    _Dijkstra(1);
    for(re int i=2;i<=n;++i)
        ans+=dis[i];
    printf("%d",ans);
    return 0;
}
