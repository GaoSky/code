//
//  P3380 【模板】二逼平衡树（树套树）.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/6/19.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 50005
#define inf 0x7fffffff
using namespace std;
int In[maxn],n,m,op,tot;
int Left,Right,Key;
int ans;
int root[maxn*30];
struct Treap
{
    int data,val,cnt,size;
    int l,r;
}a[maxn*60];
inline void Update(int x)
{
    a[x].size=a[a[x].l].size+a[a[x].r].size+a[x].cnt;
}
inline void zig(int &x)
{
    int q=a[x].l;
    a[x].l=a[q].r,a[q].r=x;
    x=q;
    Update(a[x].r),Update(x);
}
inline void zag(int &x)
{
    int q=a[x].r;
    a[x].r=a[q].l,a[q].l=x;
    x=q;
    Update(a[x].l),Update(x);
}
void insert(int &x,int val)
{
    if(x==0)
    {
        x=++tot;
        a[x].cnt=a[x].size=1;
        a[x].val=val,a[x].data=rand();
        return;
    }
    if(val==a[x].val)
    {
        a[x].cnt++,Update(x);
        return;
    }
    else if(val<a[x].val)
    {
        insert(a[x].l,val);
        if(a[x].data<a[a[x].l].data)
            zig(x);
    }
    else
    {
        insert(a[x].r,val);
        if(a[x].data<a[a[x].r].data)
            zag(x);
    }
    Update(x);
}
void _Delete(int &x,int val)
{
    if(x==0)
        return;
    if(val==a[x].val)
    {
        if(a[x].cnt>1)
        {
            a[x].cnt--,Update(x);
            return;
        }
        if(a[x].l||a[x].r)
        {
            if(a[x].r==0||a[a[x].l].data>a[a[x].r].data)
                zig(x),_Delete(a[x].r,val);
            else
                zag(x),_Delete(a[x].l,val);
            Update(x);
        }
        else
            x=0;
        return;
    }
    if(val<a[x].val)
        _Delete(a[x].l,val);
    else
        _Delete(a[x].r,val);
    Update(x);
}
void Query1(int &x,int val)
{
    if(x==0)
        return;
    if(val==a[x].val)
    {
        ans+=a[a[x].l].size;
        return;
    }
    if(val<a[x].val)
        return Query1(a[x].l,val);
    else
    {
        ans+=a[a[x].l].size+a[x].cnt;
        Query1(a[x].r,val);
    }
}
//The Segment Tree Begins Here~~~
void build(int k,int l,int r,int x,int val)
{
    insert(root[k],val);
    int mid=(l+r)>>1;
    if(l==r)
        return;
    if(x<=mid)
        build(k<<1,l,mid,x,val);
    else
        build(k<<1|1,mid+1,r,x,val);
}
void Find(int k,int l,int r,int ll,int rr,int val)
{
    if(l==ll&&r==rr)
    {
        Query1(root[k],val);
        return;
    }
    int mid=(l+r)>>1;
    if(rr<=mid)
        Find(k<<1,l,mid,ll,rr,val);
    else if(mid<ll)
        Find(k<<1|1,mid+1,r,ll,rr,val);
    else
    {
        Find(k<<1,l,mid,ll,mid,val);
        Find(k<<1|1,mid+1,r,mid+1,rr,val);
    }
}
void GetByVal(int k)
{
    int l=0,r=inf,val=0;
    while(l<=r)
    {
        int mid=(l+r)>>1;
        ans=1;
        Find(1,1,n,Left,Right,mid);
        if(ans<=k)
            l=mid+1,val=mid;
        else
            r=mid-1;
    }
    printf("%d\n",val);
}
void modify(int k,int l,int r,int x,int ori,int val)
{
    _Delete(root[k],ori);
    insert(root[k],val);
    int mid=(l+r)>>1;
    if(l==r)
        return;
    if(x<=mid)
        modify(k<<1,l,mid,x,ori,val);
    else
        modify(k<<1|1,mid+1,r,x,ori,val);
}
void PreNode(int x,int val)
{
    if(x==0)
        return;
    if(a[x].val<val)
        ans=max(ans,a[x].val),PreNode(a[x].r,val);
    else
        PreNode(a[x].l,val);
}
void GetPre(int k,int l,int r,int ll,int rr,int val)
{
    if(l==ll&&r==rr)
    {
        PreNode(root[k],val);
        return;
    }
    int mid=(l+r)>>1;
    if(rr<=mid)
        GetPre(k<<1,l,mid,ll,rr,val);
    else if(mid<ll)
        GetPre(k<<1|1,mid+1,r,ll,rr,val);
    else
    {
        GetPre(k<<1,l,mid,ll,mid,val);
        GetPre(k<<1|1,mid+1,r,mid+1,rr,val);
    }
}
void NextNode(int x,int val)
{
    if(x==0)
        return;
    if(a[x].val>val)
        ans=min(ans,a[x].val),NextNode(a[x].l,val);
    else
        NextNode(a[x].r,val);
}
void GetNext(int k,int l,int r,int ll,int rr,int val)
{
    if(l==ll&&r==rr)
    {
        NextNode(root[k],val);
        return;
    }
    int mid=(l+r)>>1;
    if(rr<=mid)
        GetNext(k<<1,l,mid,ll,rr,val);
    else if(mid<ll)
        GetNext(k<<1|1,mid+1,r,ll,rr,val);
    else
    {
        GetNext(k<<1,l,mid,ll,mid,val);
        GetNext(k<<1|1,mid+1,r,mid+1,rr,val);
    }
}
int main()
{
    srand((unsigned)time(NULL));
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
        scanf("%d",&In[i]);
    for(re int i=1;i<=n;++i)
        build(1,1,n,i,In[i]);
    for(re int i=1;i<=m;++i)
    {
        scanf("%d",&op);
        if(op==1)
        {
            scanf("%d%d%d",&Left,&Right,&Key);
            ans=1;
            Find(1,1,n,Left,Right,Key);
            printf("%d\n",ans);
        }
        else if(op==2)
        {
            scanf("%d%d%d",&Left,&Right,&Key);
            GetByVal(Key);
        }
        else if(op==3)
        {
            scanf("%d%d",&Left,&Right);
            modify(1,1,n,Left,In[Left],Right);
            In[Left]=Right;
        }
        else if(op==4)
        {
            scanf("%d%d%d",&Left,&Right,&Key);
            ans=-inf;
            GetPre(1,1,n,Left,Right,Key);
            printf("%d\n",ans);
        }
        else if(op==5)
        {
            scanf("%d%d%d",&Left,&Right,&Key);
            ans=inf;
            GetNext(1,1,n,Left,Right,Key);
            printf("%d\n",ans);
        }
    }
    return 0;
}
