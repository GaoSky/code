//
//  P1122 最大子树和.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/28.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#define re register
#define maxn 16005
using namespace std;
int n,a[maxn],f[maxn],ans;
vector<int> son[maxn];
void dfs(int x,int fa)
{
    f[x]=a[x];
    for(re int i=0;i<son[x].size();++i)
    {
        int y=son[x][i];
        if(y!=fa)
        {
            dfs(y,x);
            f[x]+=max(0,f[y]);
        }
    }
    ans=max(ans,f[x]);
}
int main()
{
    int x,y;
    scanf("%d",&n);
    for(re int i=1;i<=n;++i)
        scanf("%d",&a[i]);
    for(re int i=1;i<n;++i)
    {
        scanf("%d%d",&x,&y);
        son[x].push_back(y);
        son[y].push_back(x);
    }
    dfs(1,0);
    printf("%d",ans);
    return 0;
}
