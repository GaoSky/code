//
//  P3834 【模板】可持久化线段树 1（主席树）.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/6/13.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
#define maxn 200005
#define inf 0x7fffffff
using namespace std;
struct Tree
{
    int l,r,sum;
}tr[maxn*20];
int n,m,tot,t,a[maxn],b[maxn],root[maxn];
int build(int l,int r)
{
    int p=++tot;
    tr[p].sum=0;
    if(l==r)
        return p;
    int mid=(l+r)>>1;
    tr[p].l=build(l,mid);
    tr[p].r=build(mid+1,r);
    return p;
}
int insert(int now,int l,int r,int x,int val)
{
    int p=++tot;
    tr[p]=tr[now];
    if(l==r)
    {
        tr[p].sum+=val;
        return p;
    }
    int mid=(l+r)>>1;
    if(x<=mid)
        tr[p].l=insert(tr[now].l,l,mid,x,val);
    else
        tr[p].r=insert(tr[now].r,mid+1,r,x,val);
    tr[p].sum=tr[tr[p].l].sum+tr[tr[p].r].sum;
    return p;
}
int Query(int qr,int ql,int l,int r,int k)
{
    if(l==r)
        return l;
    int mid=(l+r)>>1;
    int lcnt=tr[tr[qr].l].sum-tr[tr[ql].l].sum;
    if(k<=lcnt)
        return Query(tr[qr].l,tr[ql].l,l,mid,k);
    else
        return Query(tr[qr].r,tr[ql].r,mid+1,r,k-lcnt);
}
int main()
{
    scanf("%d%d",&n,&m);
    for(re int i=1;i<=n;++i)
    {
        scanf("%d",&a[i]);
        b[++t]=a[i];
    }
    sort(b+1,b+t+1);
    t=unique(b+1,b+t+1)-(b+1);
    root[0]=build(1,t);
    for(re int i=1;i<=n;++i)
    {
        int x=lower_bound(b+1,b+t+1,a[i])-b;
        root[i]=insert(root[i-1],1,t,x,1);
    }
    int x,y,z;
    for(re int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        int tmp=Query(root[y],root[x-1],1,t,z);
        printf("%d\n",b[tmp]);
    }
    return 0;
}
