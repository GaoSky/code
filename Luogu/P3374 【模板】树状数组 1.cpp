//
//  main.cpp
//  luogu 3374 树状数组1
//
//  Created by 高飞 on 2018/7/30.
//  Copyright © 2018年 高飞. All rights reserved.
//

#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;
int c[500005],n,m,a[500005],x,y,z,s[500005];
int lowbit(int x)
{
    return x&-x;
}
void work()
{
    for(int i=1;i<=n;i++)
        s[i]=s[i-1]+a[i];
    for(int i=1;i<=n;i++)
        c[i]=s[i]-s[i-lowbit(i)];
}
int query(int x)
{
    int ans=0;
    while(x)
    {
        ans+=c[x];
        x=x-lowbit(x);
    }
    return ans;
}
void modify(int x,int value)
{
    while(x<=n)
    {
        c[x]+=value;
        x=x+lowbit(x);
    }
}
int main()
{
    scanf("%d%d",&n,&m);
    for(int i=1;i<=n;i++)
        scanf("%d",&a[i]);
    work();
    for(int i=1;i<=m;i++)
    {
        scanf("%d",&x);
        if(x==1)
        {
            scanf("%d%d",&y,&z);
            modify(y,z);
        }
        else if(x==2)
        {
            scanf("%d%d",&y,&z);
            printf("%d\n",query(z)-query(y-1));
        }
    }
    return 0;
}
