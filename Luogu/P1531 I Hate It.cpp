//
//  P1531 I Hate It.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/31.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <iostream>
#include <algorithm>
#define re register
#define maxn 200005
using namespace std;
struct point
{
    int l,r,val;
}tr[maxn*4];
int m,n,cnt,p[maxn];
char op[10];
void buildtree(int x,int l,int r)
{
    tr[x].l=l;
    tr[x].r=r;
    if(l==r)
    {
        tr[x].val=p[l];
        return;
    }
    int lch=x<<1,rch=x<<1|1;
    int mid=(l+r)>>1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
    tr[x].val=max(tr[lch].val,tr[rch].val);
}
void modify(int x,int l,int r,int pos,int k)
{
    if(r<pos||l>pos)
        return;
    if(l==r&&l==pos)
    {
        tr[x].val=k;
        return;
    }
    int mid=(l+r)>>1;
    modify(x<<1,l,mid,pos,k);
    modify(x<<1|1,mid+1,r,pos,k);
    tr[x].val=max(tr[x<<1].val,tr[x<<1|1].val);
}
int query(int x,int l,int r)
{
    if(l<=tr[x].l&&tr[x].r<=r)
        return tr[x].val;
    int mid=(tr[x].l+tr[x].r)>>1;
    int ans=0;
    if(l<=mid)
        ans=max(ans,query(x<<1,l,r));
    if(r>mid)
        ans=max(ans,query(x<<1|1,l,r));
    return ans;
}
int main()
{
    scanf("%d%d",&n,&m);
    for(int i=1;i<=n;i++)
        scanf("%d",&p[i]);
    buildtree(1,1,n);
    for(int i=1;i<=m;i++)
    {
        scanf("%s",op);
        if(op[0]=='U')
        {
            int a,b;
            scanf("%d%d",&a,&b);
            if(p[a]<b)
            {
                p[a]=b;
                modify(1,1,n,a,b);
            }
        }
        else
        {
            int a,b;
            scanf("%d%d",&a,&b);
            printf("%d\n",query(1,a,b));
        }
    }
    return 0;
}
