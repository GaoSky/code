//
//  P1816 忠诚.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/7/31.
//  Copyright © 2020 SkyGao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <iostream>
#define re register
#define maxn 100005
using namespace std;
struct Node
{
    int l,r,minn;
}tr[maxn*4];
int m,n,a[maxn];
void buildtree(int x,int l,int r)
{
    tr[x].l=l,tr[x].r=r;
    if(l==r)
    {
        tr[x].minn=a[l];
        return;
    }
    int lch=x<<1,rch=x<<1|1;
    int mid=(l+r)>>1;
    buildtree(lch,l,mid);
    buildtree(rch,mid+1,r);
    tr[x].minn=min(tr[lch].minn,tr[rch].minn);
}
int query(int x,int l,int r)
{
    if(l<=tr[x].l&&tr[x].r<=r)
        return tr[x].minn;
    int mid=(tr[x].l+tr[x].r)>>1;
    int ans=0x7fffffff;
    if(l<=mid)
        ans=min(ans,query(x<<1,l,r));
    if(r>mid)
        ans=min(ans,query(x<<1|1,l,r));
    return ans;
}
int main()
{
    scanf("%d%d",&m,&n);
    for(re int i=1;i<=m;++i)
        scanf("%d",&a[i]);
    buildtree(1,1,m);
    int x,y;
    for(re int i=1;i<=n;++i)
    {
        scanf("%d%d",&x,&y);
        printf("%d ",query(1,x,y));
    }
    return 0;
}
