//
//  P7075 儒略日.cpp
//  Algorithm
//
//  Created by SkyGao on 2020/11/10.
//  Copyright © 2020 skygao. All rights reserved.
//

#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#define re register
using namespace std;
int days[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};
inline bool check(int x)
{
    return (x%4==0&&x%100!=0)||(x%400==0);
}
int main()
{
    int q,year,month,day;
    long long n;
    bool flag;
    scanf("%d",&q);
    while(q--)
    {
        scanf("%lld",&n);
        year=-4713,month=1,day=1,flag=false;
        if(n<=2299160)
        {
            flag=true;
            year+=4*(n/1461);
            n%=1461;
            if(n>=366)
            {
                flag=false;
                n-=366,year++;
                while(n>=365)
                    n-=365,year++;
            }
            for(re int i=1;i<=12;++i)
            {
                if(days[i]+(i==2&&flag)>n)
                    break;
                n-=(days[i]+(i==2&&flag)),month++;
            }
            day+=n;
            if(year>=0)
                year++;
            if(year<0)
                printf("%d %d %d BC\n",day,month,-year);
            else
                printf("%d %d %d\n",day,month,year);
        }
        else
        {
            year=1582,month=10,day=15;
            n-=2299161;
            if(n<=16)
                day+=n;
            else if(n<=77)
            {
                n-=17,day=1,month++;
                for(re int i=11;i<=12;++i)
                {
                    if(days[i]>n)
                        break;
                    n-=days[i],month++;
                }
                day+=n;
            }
            else
            {
                n-=78;
                year++,month=1,day=1;
                year+=400*(n/146097);
                n%=146097;
                while(n>=365+check(year))
                    n-=(365+check(year)),year++;
                flag=check(year);
                for(re int i=1;i<=12;++i)
                {
                    if(days[i]+(i==2&&flag)>n)
                        break;
                    n-=(days[i]+(i==2&&flag)),month++;
                }
                day+=n;
            }
            printf("%d %d %d\n",day,month,year);
        }
    }
    return 0;
}
